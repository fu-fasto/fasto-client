const path = require("path")
const rewireAliases = require("react-app-rewire-aliases")

module.exports = config => {
  require("react-app-rewire-postcss")(config, {
    plugins: loader => [require("postcss-rtl")()]
  })

  config = rewireAliases.aliasesOptions({
    "@src": path.resolve(__dirname, "src"),
    "@fasto": path.resolve(__dirname, "src/components/@fasto")
  })(config)

  return config
}
