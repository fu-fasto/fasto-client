import React from "react"
import * as Icon from "react-feather"
const navigationConfig = [
  {
    id: "home",
    title: "Home",
    type: "item",
    icon: <Icon.Home size={20} />,
    navLink: "/"
  },
  {
    id: "page2",
    title: "Page Test",
    type: "item",
    icon: <Icon.File size={20} />,
    permissions: ["ADMIN"],
    navLink: "/page2"
  },
  {
    id: "test-table",
    title: "Table Test",
    type: "item",
    icon: <Icon.File size={20} />,
    permissions: ["ADMIN"],
    navLink: "/test-table"
  },
  {
    id: "users",
    title: "User",
    type: "collapse",
    permissions: ["ADMIN"],
    icon: <Icon.User size={20} />,
    children: [
      {
        id: "create",
        title: "Create",
        type: "item",
        icon: <Icon.Circle size={12} />,
        // permissions: ["ADMIN"],
        navLink: "/user/create"
      },
      {
        id: "list",
        title: "List",
        type: "item",
        icon: <Icon.Circle size={12} />,
        // permissions: ["ADMIN"],
        navLink: "/user/list"
      }
    ]
  },
  {
    id: "userManagement",
    title: "User Management",
    type: "item",
    permissions: ["FASTO.ADMIN", "STORE.ADMIN"],
    icon: <Icon.User size={20} />,
    navLink: "/users"
  },
  {
    id: "menuManagement",
    title: "Menu Management",
    icon: <Icon.List size={20} />,
    type: "item",
    navLink: "/menu-management",
    permissions: ["ADMIN"]
  },
  {
    id: "store",
    title: "Manage Store",
    type: "item",
    icon: <Icon.Cpu size={20} />,
    permissions: ["STORE.ADMIN", "FASTO.ADMIN"],
    navLink: "/app/store/view"
  },
  {
    id: "reservations",
    title: "Reservations",
    type: "item",
    icon: <Icon.Award size={20} />,
    permissions: ["STORE.ADMIN", "STORE.STAFF", "CUSTOMER"],
    navLink: "/reservations"
  },
  {
    id: "visits",
    title: "Visits",
    type: "item",
    icon: <Icon.Activity size={20} />,
    permissions: ["STORE.ADMIN", "STORE.STAFF", "CUSTOMER"],
    navLink: "/visits"
  },
  {
    id: "ordersManager",
    title: "Orders",
    type: "item",
    badge: "warning",
    icon: <Icon.Coffee size={20} />,
    permissions: ["STORE.ADMIN", "STORE.STAFF"],
    navLink: "/orders-manager"
  },
  {
    id: "payment-account",
    title: "Payment Accounts",
    type: "item",
    icon: <Icon.CreditCard size={20} />,
    permissions: ["STORE.ADMIN"],
    navLink: "/payment-accounts"
  },
  {
    id: "payment-transactions",
    title: "Payment History",
    type: "item",
    icon: <Icon.List size={20} />,
    permissions: ["CUSTOMER", "STORE.ADMIN", "STORE.STAFF"],
    navLink: "/payment-transactions"
  }
]

export default navigationConfig
