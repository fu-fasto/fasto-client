/* eslint-disable react/jsx-no-comment-textnodes */
import React, { Suspense, lazy } from "react"
import { Router, Switch, Route } from "react-router-dom"
import { history } from "./history"
import { connect } from "react-redux"
import Spinner from "./components/@vuexy/spinner/Loading-spinner"
import UserSetup from "./components/@fasto/UserSetup"
import { ContextLayout } from "./utility/context/Layout"

// Route-based code splitting
const Home = lazy(() => import("./views/apps/store/StoreHomePage"))

const login = lazy(() => import("./views/pages/authentication/Login"))
const logout = lazy(() => import("./views/pages/authentication/Logout"))

const userView = lazy(() => import("./views/apps/user/view/View"))

const userCreate = lazy(() => import("./views/apps/user/CreateUser"))

const userList = lazy(() => import("./views/apps/user/ListUser"))

const userUpdate = lazy(() => import("./views/apps/user/UpdateUser"))

const changePassword = lazy(() => import("./views/apps/user/ChangePwd"))

const currUserInfo = lazy(() => import("./views/apps/user/CurrUserInfo"))

const menuList = lazy(() => import("./views/apps/menu/list/List"))

//Store
const storeManagement = lazy(() => import("./views/apps/store/Store"))

const storeDetail = lazy(() => import("./views/apps/store/StoreDetail"))

const storeForm = lazy(() => import("./views/apps/store/StoreForm"))

const addUpdateItemForm = lazy(() =>
  import("./views/apps/store/AddUpdateItemForm")
)

const listItemTable = lazy(() => import("./views/apps/store/MenuItemList"))

const searchResultPage = lazy(() =>
  import("./views/apps/store/SearchResultPage")
)

// Reservations
const listReservation = lazy(() =>
  import("./views/apps/reservation/ListReservation")
)
const makeReservation = lazy(() =>
  import("./views/apps/reservation/MakeReservation")
)
const updateReservationState = lazy(() =>
  import("./views/apps/reservation/UpdateReservationState")
)

// Visits
const listVisit = lazy(() => import("./views/apps/visit/ListVisit"))
const checkInVisit = lazy(() => import("./views/apps/visit/CheckInVisit"))
const checkInVisitWithQrCode = lazy(() => import("./views/apps/visit/CheckInVisitWithQrCode"))
const getCheckInQrCode = lazy(() => import("./views/apps/visit/GetCheckInQrCode"))

// Orders
const listOrders = lazy(() => import("./views/apps/order/ListOrder"))
const makeOrder = lazy(() => import("./views/apps/order/MakeOrder"))
const updateOrder = lazy(() => import("./views/apps/order/UpdateOrderState"))
const storeListOrder = lazy(() => import("./views/apps/order/StoreListOrder"))

// Checkout
const checkout = lazy(() => import("./views/apps/checkout/Checkout.js"))

// Payment Account
const paymentAccountList = lazy(() =>
  import("./views/apps/payment-account/PaymentAccountList")
)
const updatePaymentAccount = lazy(() =>
  import("./views/apps/payment-account/UpdatePaymentAccount")
)
const createPaymentAccount = lazy(() =>
  import("./views/apps/payment-account/CreatePaymentAccount")
)

// Payment Transaction
const paymentTransactionList = lazy(() =>
  import("./views/apps/payment-transaction/PaymentTransactionList")
)

// Set Layout and Component Using App Route
const RouteConfig = ({
  component: Component,
  fullLayout,
  permission,
  user,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      return (
        <ContextLayout.Consumer>
          {context => {
            let LayoutTag =
              fullLayout === true
                ? context.fullLayout
                : context.state.activeLayout === "horizontal"
                ? context.horizontalLayout
                : context.VerticalLayout
            return (
              <LayoutTag {...props} permission={props.role}>
                <Suspense fallback={<Spinner />}>
                  <Component {...props} />
                </Suspense>
              </LayoutTag>
            )
          }}
        </ContextLayout.Consumer>
      )
    }}
  />
)
const mapStateToProps = state => {
  return {
    role: state.auth.role
  }
}

const AppRoute = connect(mapStateToProps)(RouteConfig)

class AppRouter extends React.Component {
  render() {
    return (
      // Set the directory path if you are deploying in sub-folder
      <Router history={history}>
        <UserSetup />
        <Switch>
          <AppRoute exact path="/" component={Home} />
          <AppRoute path="/login" component={login} fullLayout />
          <AppRoute path="/logout" component={logout} />
          // Users
          <AppRoute path="/edit-info" component={currUserInfo} />
          <AppRoute path="/users" component={userList} />
          <AppRoute path="/create-user" component={userCreate} />
          <AppRoute path="/update-user/:id" component={userUpdate} />
          <AppRoute path="/change-pwd" component={changePassword} />
          <AppRoute path="/menu-management" component={menuList} />
          //store
          <AppRoute path="/app/user/store" component={userView} />
          <AppRoute path="/app/store/view" component={storeManagement} />
          <AppRoute path="/app/store/detail/:id" component={storeDetail} />
          <AppRoute path="/app/store/edit" component={storeForm} />
          <AppRoute path="/app/store/create" component={storeForm} />
          <AppRoute
            path="/app/store/addUpdateItemForm"
            component={addUpdateItemForm}
          />
          <AppRoute
            path="/app/store/createItem"
            component={addUpdateItemForm}
          />
          <AppRoute
            path="/app/store/listItemTable/:id"
            component={listItemTable}
          />
          <AppRoute path="/app/store/search" component={searchResultPage} />
          // Reservations
          <AppRoute path="/reservations" component={listReservation} />
          <AppRoute path="/make-reservation/:id" component={makeReservation} />
          <AppRoute
            path="/update-reservation-state/:id"
            component={updateReservationState}
          />
          // Visits
          <AppRoute path="/visits" component={listVisit} />
          <AppRoute path="/check-in-visit" component={checkInVisit} />
          <AppRoute path="/check-in-qr" component={checkInVisitWithQrCode} />
          <AppRoute path="/get-qr-code" component={getCheckInQrCode} />
          // Orders
          <AppRoute path="/orders" component={listOrders} />
          <AppRoute path="/update-order-state/:id" component={updateOrder} />
          <AppRoute path="/make-order" component={makeOrder} />
          <AppRoute path="/orders-manager" component={storeListOrder} />
          // Checkout
          <AppRoute path="/checkout" component={checkout} />
          // Payment Account
          <AppRoute path="/payment-accounts" component={paymentAccountList} />
          <AppRoute
            path="/update-payment-account/:id"
            component={updatePaymentAccount}
          />
          <AppRoute
            path="/create-payment-account"
            component={createPaymentAccount}
          />
          // Payment Transactions
          <AppRoute
            path="/payment-transactions"
            component={paymentTransactionList}
          />
        </Switch>
      </Router>
    )
  }
}

export default AppRouter
