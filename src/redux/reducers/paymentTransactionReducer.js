const initialState = {
  itemList: [],
  totalPrice: 0,
  details: {},
  paymentTransactions: {}
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "GET_ITEM_LIST":
      const itemList = action.itemList
      return {
        ...state,
        itemList
      }

    case "CALCULATE_TOTAL_PRICE":
      const result = state.itemList.reduce((totalPrice, item) => {
        return totalPrice + item.price * item.quantity
      }, 0)
      return {
        ...state,
        totalPrice: result
      }

    case "INIT_DETAILS":
      const details = {
        ...action.details,
        price: state.totalPrice
      }
      return {
        ...state,
        details
      }

    case "GET_ALL_PAYMENT_TRANSACTIONS":
      const paymentTransactions = action.paymentTransactions.reduce(
        (acc, x) => {
          acc[x.id] = x
          return acc
        },
        {}
      )
      return {
        ...state,
        paymentTransactions: paymentTransactions
      }

    default:
      return state
  }
}

export default reducer
