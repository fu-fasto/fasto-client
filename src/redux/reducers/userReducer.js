const initialState = {
  users: {},
  roles: {}
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "GET_USERS": {
      return {
        ...state,
        users: action.users
      }
    }

    case "GET_ROLES": {
      return {
        ...state,
        roles: action.roles
      }
    }

    case "UPDATE_USER": {
      const newUsers = state.users.map(user =>
        user.id !== action.user.id ? user : action.user
      )
      return {
        ...state,
        users: newUsers
      }
    }

    case "CHANGE_PASSWORD": {
      return {
        ...state
      }
    }

    default:
      return state
  }
}

export default reducer
