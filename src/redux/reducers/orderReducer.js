const initialState = {
  orders: {},
  ordersByItem: {}
}

const updateCurrentState = order => {
  order.state = order.states[order.states.length - 1].state
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "INIT_ORDERS": { 
      return {
        ...state,
        orders: action.orders
      }
    }
    
    case "CLEAR_VISIT_ORDERS": {
      const newOrders = {}
      Object.values(state.orders).forEach(order => {
        if(order.visitId !== action.visitId){
          newOrders[order.id] = order
        }
      })
      return {
        ...state,
        orders: newOrders
      }
    }

    case "MAKE_ORDER": {
      updateCurrentState(action.order)
      return {
        ...state,
        orders: {
          [action.order.id]: action.order,
          ...state.orders
        }
      }
    }

    case "UPDATE_ORDER":
      const order = state.orders[action.order.id]

      return {
        ...state,
        orders: {
          ...state.orders,
          [order.id]: {
            ...order,
            processing: action.order.processing,
            note: action.order.note
          }
        }
      }

    case "UPDATE_ORDER_REALTIME": {
      const order = state.orders[action.order.id] || action.order

      return {
        ...state,
        orders: {
          ...state.orders,
          [order.id]: {
            ...order,
            ...action.order,
            processing: false
          }
        }
      }
    }
    default:
      return state
  }
}

export default reducer
