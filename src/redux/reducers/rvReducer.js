const initialState = {
  reservations: {},
  visits: {},
  qrCodeResult: {}
}

const updateCurrentState = reservation => {
  reservation.state = reservation.states[reservation.states.length - 1].state
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "GET_RESERVATIONS": {
      const reservations = {}
      action.reservations.forEach(updateCurrentState)
      action.reservations.forEach(r => {
        reservations[r.id] = r
      })

      return {
        ...state,
        reservations
      }
    }

    case "MAKE_RESERVATION": {
      updateCurrentState(action.reservation)
      state.reservations[action.reservation.id] = action.reservation

      return {
        ...state
      }
    }

    case "UPDATE_RESERVATION":
      const reservation = state.reservations[action.reservation.id]

      return {
        ...state,
        reservations: {
          ...state.reservations,
          [reservation.id]: {
            ...reservation,
            processing: action.reservation.processing,
            note: action.reservation.note
          }
        }
      }

    case "UPDATE_RESERVATION_REALTIME": {
      const reservation =
        state.reservations[action.reservation.id] || action.reservation

      return {
        ...state,
        reservations: {
          ...state.reservations,
          [reservation.id]: {
            ...reservation,
            ...action.reservation,
            processing: false
          }
        }
      }
    }

    case "CLEAR_ONE_VISIT":{
      const newVisits = {}
      Object.keys(state.visits).forEach(visitId => {
        if(visitId !== action.visitId){
          newVisits[visitId] = state.visits[visitId]
        }
      })
      return {
        ...state,
        visits: newVisits
      }
    }

    case "GET_VISITS": {
      const visits = action.visits.reduce((acc, x) => {
        acc[x.id] = x
        return acc
      }, {})

      return {
        ...state,
        visits
      }
    }

    case "ADD_OR_UPDATE_VISIT": {
      const visit = state.visits[action.visit.id] ?? action.visit

      return {
        ...state,
        visits: {
          ...state.visits,
          [visit.id]: {
            ...visit,
            ...action.visit,
            processing: action.visit?.processing === true
          }
        }
      }
    }

    case "UPDATE_QR_CODE_RESULT": {
      return {
        ...state,
        qrCodeResult: action.qrCodeResult
      }
    }

    default:
      return state
  }
}

export default reducer
