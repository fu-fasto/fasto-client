import { combineReducers } from "redux"
import customizer from "./customizer/"
import authReducer from "./authReducer"
import navbar from "./navbar/Index"
import dataList from "./data-list/"
import rv from "./rvReducer"
import user from "./userReducer"
import order from "./orderReducer"
import paymentTransaction from "./paymentTransactionReducer"
import paymentAccount from "./paymentAccountReducer"

const rootReducer = combineReducers({
  customizer: customizer,
  auth: authReducer,
  navbar: navbar,
  dataList: dataList,
  rv: rv,
  user: user,
  order: order,
  paymentTransaction: paymentTransaction,
  paymentAccount: paymentAccount
})

export default rootReducer
