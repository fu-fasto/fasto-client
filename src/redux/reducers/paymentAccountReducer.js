const initialState = {
  list: []
}

const reducer = (state = initialState, actions) => {
  switch (actions.type) {
    case "GET_PAYMENT_ACCOUNTS":
      const paymentAccounts = actions.paymentAccounts
      return {
        ...state,
        list: paymentAccounts
      }

    case "UPDATE_PAYMENT_ACCOUNT":
      const newPaymentAccounts = state.list.map(account =>
        account.id !== actions.id ? account : { ...account, ...actions.values }
      )
      return {
        ...state,
        newPaymentAccounts
      }

    case "CREATE_PAYMENT_ACCOUNT":
      const newList = [...state.list, actions.paymentAccount]
      return {
        ...state,
        list: newList
      }

    default:
      return state
  }
}

export default reducer
