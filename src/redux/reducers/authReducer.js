import JwtService from "@src/utility/auth/jwt/jwtService"
JwtService.initInterceptors()

const getInitialState = () => {
  try {
    const initialState = JSON.parse(localStorage.getItem("auth"))

    if (!initialState.id) {
      throw new Error()
    }

    return initialState
  } catch (error) {
    localStorage.clear()
    return {
      name: "Guest",
      role: "GUEST"
    }
  }
}

const reducer = (state = getInitialState(), action) => {
  switch (action.type) {
    case "LOGIN":
      localStorage.setItem("auth", JSON.stringify(action.loginResponse))

      return {
        ...action.loginResponse
      }

    case "LOGOUT":
      localStorage.clear()

      return {
        ...getInitialState()
      }

    default:
      return state
  }
}

export default reducer
