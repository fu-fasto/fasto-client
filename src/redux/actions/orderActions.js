import orderService from "../../services/orderService"

export const makeOrder = order => async dispatch => {
  const id = await orderService.makeOrder(order)
  const newOrder = {
    ...order,
    id: id,
    states: [
      {
        state: 0
      }
    ]
  }

  return dispatch({
    type: "MAKE_ORDER",
    order: newOrder
  })
}

export const updateOrder = order => async dispatch => {
  await orderService.updateOrder(order)

  return dispatch({
    type: "UPDATE_ORDER",
    order
  })
}

export const updateOrderRealtime = order => async dispatch => {
  return dispatch({
    type: "UPDATE_ORDER_REALTIME",
    order
  })
}

export const clearVisitOrders = visitId => async dispatch => {
  return dispatch({
    type: "CLEAR_VISIT_ORDERS",
    visitId
  })
}
