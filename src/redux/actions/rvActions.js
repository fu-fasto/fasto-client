import rvService from "../../services/rvService"
import orderService from "../../services/orderService"

export const getReservations = (findQuery = {}) => async dispatch => {
  const reservations = await rvService.getReservations(findQuery)
  if (!reservations.length) return

  return dispatch({
    type: "GET_RESERVATIONS",
    reservations
  })
}

export const getVisits = (findQuery = {}) => async dispatch => {
  const visits = await rvService.getVisits(findQuery)
  if (!visits.length) return
  const allOrders = {}

  // Populate orders foreach visit
  await Promise.all(
    visits.map(async visit => {
      const orders = await orderService.getOrders({ visitId: visit.id })
      orders.forEach(order => (allOrders[order.id] = order))
    })
  )

  await dispatch({
    type: "INIT_ORDERS",
    orders: allOrders
  })

  return dispatch({
    type: "GET_VISITS",
    visits
  })
}

export const makeReservation = reservation => async dispatch => {
  const id = await rvService.makeReservation(reservation)
  const newReservation = {
    ...reservation,
    id: id,
    states: [
      {
        state: 0,
        note: reservation.note
      }
    ]
  }

  return dispatch({
    type: "MAKE_RESERVATION",
    reservation: newReservation
  })
}

export const updateReservation = reservation => async dispatch => {
  await rvService.updateReservation(reservation)

  return dispatch({
    type: "UPDATE_RESERVATION",
    reservation
  })
}

export const updateReservationRealtime = reservation => async dispatch => {
  return dispatch({
    type: "UPDATE_RESERVATION_REALTIME",
    reservation
  })
}

export const checkInVisit = visit => async dispatch => {
  const id = await rvService.checkInVisit(visit)

  return dispatch({
    type: "ADD_OR_UPDATE_VISIT",
    visit: {
      ...visit,
      id,
      processing: true
    }
  })
}

export const checkOutVisit = visit => async dispatch => {
  await rvService.checkOutVisit(visit)

  return dispatch({
    type: "ADD_OR_UPDATE_VISIT",
    visit: {
      ...visit,
      processing: true
    }
  })
}

export const clearOneVisit = visitId => dispatch => {
  return dispatch({
    type: "CLEAR_ONE_VISIT",
    visitId
  })
}

export const updateVisitRealtime = visit => async dispatch => {
  return dispatch({
    type: "ADD_OR_UPDATE_VISIT",
    visit: { ...visit, processing: false }
  })
}

export const updateQrCodeResult = qrCodeResult => dispatch => {
  return dispatch({
    type: "UPDATE_QR_CODE_RESULT",
    qrCodeResult
  })
}
