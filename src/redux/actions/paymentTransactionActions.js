import service from "../../services/paymentService"

export const initPaymentTransactionInfo = () => async dispatch => {
  const itemList = await service.getItemList()
  if (!itemList.length) return

  dispatch({
    type: "GET_ITEM_LIST",
    itemList
  })
  dispatch({
    type: "CALCULATE_TOTAL_PRICE"
  })
  dispatch({
    type: "INIT_DETAILS",
    details: {
      currency: "USD",
      customerId: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      storeId: "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    }
  })
}

export const getAllPaymentTransactions = findQuery => async dispatch => {
  const paymentTransactions = await service.getAllPaymentTransactions(findQuery)
  dispatch({
    type: "GET_ALL_PAYMENT_TRANSACTIONS",
    paymentTransactions
  })
}
