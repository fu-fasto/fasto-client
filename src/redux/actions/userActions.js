import userService from "../../services/userService"

export const getUsers = currUser => async dispatch => {
  const users = await userService.getUsers(currUser)
  if (!users.length) return

  const newUsersPromises = users.map(async user => ({
    ...user,
    role: await userService.getRole(user.roleId)
  }))
  const newUsers = await Promise.all(newUsersPromises)
  return dispatch({
    type: "GET_USERS",
    users: newUsers
  })
}

export const createUser = user => async dispatch => {
  const id = await userService.createUser(user)
  const newUser = {
    id: id,
    ...user
  }

  return dispatch({
    type: "CREATE_USER",
    user: newUser
  })
}

export const updateUser = user => async dispatch => {
  await userService.updateUser(user)

  return dispatch({
    type: "UPDATE_USER",
    user
  })
}

export const getRoles = currUser => async dispatch => {
  const roles = await userService.getRoles(currUser)
  return dispatch({
    type: "GET_ROLES",
    roles
  })
}

export const changePassword = params => async dispatch => {
  const user = await userService.changePassword(params)
  return dispatch({
    type: "CHANGE_PASSWORD",
    user
  })
}
