export const login = loginResponse => dispatch => {
  return dispatch({
    type: "LOGIN",
    loginResponse
  })
}

export const logout = () => dispatch => {
  return dispatch({
    type: "LOGOUT"
  })
}
