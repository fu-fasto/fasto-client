import paymentService from "../../services/paymentService"

//TODO: get StoreId from API or whatever, hardcoded for now
export const getPaymentAccounts = () => async dispatch => {
  const paymentAccounts = await paymentService.getPaymentAccounts({
    storeId: "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  })
  if (!paymentAccounts) return
  return dispatch({
    type: "GET_PAYMENT_ACCOUNTS",
    paymentAccounts
  })
}

export const updatePaymentAccount = (id, values) => async dispatch => {
  await paymentService.updatePaymentAccount(id, values)
  return dispatch({
    type: "UPDATE_PAYMENT_ACCOUNT",
    values,
    id
  })
}

export const createPaymentAccount = data => async dispatch => {
  const paymentAccount = {
    ...data,
    storeId: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    isActivated: true
  }
  paymentAccount.id = await paymentService.createPaymentAccount(
    paymentAccount
  )
  return dispatch({
    type: "CREATE_PAYMENT_ACCOUNT",
    paymentAccount
  })
}
