import axios from "../utility/axios"
const BASE_URL = process.env.REACT_APP_STORE_SEARCH_MICROSERVICE || null

const endpoint = {
  getAll: BASE_URL + "/items/",
  create: BASE_URL + "/items/",
  edit: BASE_URL + "/items/",
  getAllByStoreId: BASE_URL + "/items/",
  getOneItem: BASE_URL + "/items/"
}

const getAllFoodRegion = () => {
  return axios.get(endpoint.getAll)
}

const createOneItem = item => {
  return axios.post(endpoint.create, item)
}

const editItem = item => {
  return axios.put(endpoint.edit.concat(item.id), item)
}

const getAllByStoreId = id => {
  return axios.get(endpoint.getAllByStoreId, { StoreId: id })
}

const getOneItem = async id => {
  return (await axios.get(endpoint.getOneItem.concat(id)))?.data
}

export default {
  getAllFoodRegion,
  createOneItem,
  editItem,
  getAllByStoreId,
  getOneItem
}