import axios from "../utility/axios"
const baseURL = process.env.REACT_APP_RV_MICROSERVICE ?? "http://localhost:5004"
// const baseURL = "http://localhost:5004"

const getReservations = async ({
  storeId = null,
  customerId = null,
  rawSorts = []
}) => {
  const params = {
    rawSorts,
    storeId,
    customerId
  }
  return (await axios.get(`${baseURL}/reservations`, params))?.data
}

const getVisits = async (findQuery = {}) => {
  return (await axios.get(`${baseURL}/visits`, findQuery))?.data
}

const getCheckInQrCode = async (query = {}) => {
  return (await axios.get(`${baseURL}/visits/check-in-qr`, query))?.data
}

const makeReservation = async reservation => {
  return (await axios.post(`${baseURL}/reservations`, reservation))?.data
}

const updateReservation = async reservation => {
  reservation.state = +reservation.state
  return (
    await axios.patch(`${baseURL}/reservations/${reservation.id}`, reservation)
  ).data
}

const checkInVisit = async visit => {
  return (await axios.post(`${baseURL}/visits`, visit))?.data
}

const checkOutVisit = async visit => {
  return (await axios.patch(`${baseURL}/visits/${visit.id}/checkout`, visit))
    ?.data
}

const createPaymentTransaction = async (visitId, provider) => {
  return axios
    .post(`${baseURL}/visits/${visitId}/create-payment-transaction`, {
      provider
    })
    .then(({ data }) => data)
    .catch(({ response }) => response.data)
}

const getOneVisit = async id => {
  return (await axios.get(`${baseURL}/visits/${id}`))?.data
}

export default {
  getReservations,
  makeReservation,
  updateReservation,
  getVisits,
  checkInVisit,
  checkOutVisit,
  getOneVisit,
  createPaymentTransaction,
  getCheckInQrCode
}
