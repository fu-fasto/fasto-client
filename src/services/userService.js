import axios from "../utility/axios"
const baseURL = process.env.REACT_APP_USER_MICROSERVICE || null

const getUsers = async ({ storeId = null }) => {
  const params = {
    storeId
  }
  return (await axios.get(`${baseURL}/users`, params)).data
}

const getUser = async (id) => {
  return (await axios.get(`${baseURL}/users/`, id)).data
}

const createUser = async user => {
  return (await axios.post(`${baseURL}/users`, user)).data
}

const updateUser = async user => {
  return (await axios.patch(`${baseURL}/users`, user)).data
}

const getRole = async id => {
  return (await axios.get(`${baseURL}/roles/${id}`)).data.roleName
}

const getRoles = async ({ storeId = null }) => {
  const params = {
    storeId
  }
  return (await axios.get(`${baseURL}/roles`, params)).data
}

const changePassword = async params => {
  return (await axios.patch(`${baseURL}/users/change-pwd`, params)).data
}

export default {
  getUsers,
  getUser,
  createUser,
  updateUser,
  getRole,
  getRoles,
  changePassword
}
