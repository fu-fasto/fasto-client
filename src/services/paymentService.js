import axios from "../utility/axios"
const baseURL = process.env.REACT_APP_PAYMENT_MICROSERVICE ?? "http://localhost:5006"
// const baseURL = "http://localhost:5006"

const getPaymentTransactions = async findQuery => {
  return (await axios.get(`${baseURL}/payment-transactions`, findQuery))?.data
}

const getOnePaymentTransaction = async id => {
  return (await axios.get(`${baseURL}/payment-transactions/${id}`))?.data
}

const createPaymentTransaction = async details => {
  details.createAt = new Date().toJSON()
  return (await axios.post(`${baseURL}/payment-transactions`, details)).data
}

const completePaymentTransaction = async id => {
  return (await axios.post(`${baseURL}/payment-transactions/complete/${id}`))
    .status
}

const getItemList = async () => {
  return (await axios.get(`/visit/item-list`)).data
}

const getAllPaymentTransactions = async findQuery => {
  return (await axios.get(`${baseURL}/payment-transactions`, findQuery))?.data
}

const getPaymentAccounts = async (query = {}) => {
  return (await axios.get(`${baseURL}/payment-accounts`, query)).data
}

const getOnePaymentAccount = async (id) => {
  return (await axios.get(`${baseURL}/payment-accounts/${id}`))?.data
}

const updatePaymentAccount = async (id, values) => {
  return (await axios.patch(`${baseURL}/payment-accounts/${id}`, values)).data
}

const createPaymentAccount = async data => {
  return (await axios.post(`${baseURL}/payment-accounts`, data)).data
}

export default {
  getPaymentTransactions,
  getOnePaymentTransaction,
  createPaymentTransaction,
  completePaymentTransaction,
  getItemList,
  getAllPaymentTransactions,
  getPaymentAccounts,
  getOnePaymentAccount,
  updatePaymentAccount,
  createPaymentAccount
}
