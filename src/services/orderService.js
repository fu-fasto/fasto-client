import axios from "../utility/axios"
const baseURL = process.env.REACT_APP_ORDER_MICROSERVICE ?? "https://localhost:5005"
// const baseURL = "http://localhost:5005"

const getOrders = async (findQuery = {}) => {
  return (await axios.get(`${baseURL}/orders`, findQuery))?.data
}

const makeOrder = async order => {
  return (await axios.post(`${baseURL}/orders`, order))?.data
}

const updateOrder = async order => {
  order.state = +order.state
  return (await axios.patch(`${baseURL}/orders`, order)).data
}

export default {
  getOrders,
  makeOrder,
  updateOrder
}
