/* eslint-disable no-unused-vars */
import * as signalR from "@microsoft/signalr"
import {
  updateReservationRealtime,
  updateVisitRealtime,
  clearOneVisit
} from "../redux/actions/rvActions"
import {
  updateOrderRealtime,
  clearVisitOrders
} from "../redux/actions/orderActions"

const URL = process.env.REACT_APP_NOTIFICATION_MICROSERVICE ?? "http://localhost:6969/hub"
// const URL = "http://localhost:6969/hub"
let connection
let dispatch
let accessToken

export const newConnection = async () => {
  if (connection) {
    await connection.stop()
  }
  accessToken = localStorage.getItem("accessToken")
  connection = new signalR.HubConnectionBuilder()
    .withUrl(URL, {
      skipNegotiation: true,
      transport: signalR.HttpTransportType.WebSockets,
      accessTokenFactory: () => accessToken
    })
    .withAutomaticReconnect([0, 5000, 10000, 20000, 60000, 300000])
    .configureLogging(signalR.LogLevel.None)
    .build()

  while (true) {
    try {
      await connection.start()
      await registerEventHandlers(connection)

      console.log("SignalR Connected.")
      break
    } catch (e) {
      await new Promise(resolve => setTimeout(resolve, 10000))
    }
  }
}

export const initConnection = initDispatch => {
  // Just init one time
  if (dispatch) return
  dispatch = initDispatch

  newConnection()
}

const registerEventHandlers = connection => {
  // Configuration
  connection.serverTimeoutInMilliseconds = 300 * 1000

  // Event handlers
  connection.on("ReservationCreated", reservation => {
    dispatch(updateReservationRealtime(reservation))
  })

  connection.on("ReservationUpdated", reservation => {
    dispatch(updateReservationRealtime(reservation))
  })

  connection.on("VisitCheckedIn", visit => {
    dispatch(updateVisitRealtime(visit))
  })

  connection.on("VisitCheckedOut", visit => {
    dispatch(updateVisitRealtime(visit))
    dispatch(clearVisitOrders(visit.id))
    dispatch(clearOneVisit(visit.id))
  })

  connection.on("OrderCreated", order => {
    dispatch(updateOrderRealtime(order))
  })

  connection.on("OrderUpdated", order => {
    dispatch(updateOrderRealtime(order))
  })

  connection.on("PaymentTransactionCompleted", paymentTransactionCompleted => {
    dispatch(
      updateVisitRealtime({
        id: paymentTransactionCompleted.visitId,
        paymentTransactionId: paymentTransactionCompleted.paymentTransactionId,
        paymentTransactionState: true
      })
    )
  })
}
