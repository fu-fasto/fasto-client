import axios from "../utility/axios"
const BASE_URL = process.env.REACT_APP_STORE_MICROSERVICE || null
const BASE_URL_SEARCH = process.env.REACT_APP_STORE_SEARCH_MICROSERVICE || null
const endpoint = {
    getAll: BASE_URL_SEARCH + "/stores/browse",
    getOne: BASE_URL_SEARCH + "/stores/",
    add: BASE_URL + "/store/createStore",
    addDetail: BASE_URL + "/store/addInfo",
    edit: BASE_URL + "/store/",
    delete: BASE_URL + "/store/",
    search: BASE_URL_SEARCH + "/stores/browse",
    metadata: BASE_URL_SEARCH + "/stores/metadata"
}

const getAllStore = () => {
    return axios.get(endpoint.getAll, { size: 20 });
}

const getOneStore = (id) => {
    return axios.get(endpoint.getOne.concat(id));
}

const addStore = (store) => {
    return axios.post(endpoint.add, store);
}

const editStore = (store) => {
    return axios.put(endpoint.edit.concat(store.id), store);
}

const deleteStore = (id) => {
    return axios.delete(endpoint.delete.concat(id));
}

const searchStore = (findQuery) => {
    return axios.get(endpoint.search,
        {
            text: findQuery?.text,
            size: 20,
            page: findQuery?.page,
            cityId: findQuery?.cityId,
            districtId: findQuery?.districtId,
            cuisines: findQuery?.cuisines,
            categories: findQuery?.categories
        }
    );
}

const getMetadata = (findQuery) => {
    return axios.get(endpoint.metadata, findQuery);
}

export default {
    getAllStore,
    getOneStore,
    addStore,
    editStore,
    deleteStore,
    searchStore,
    getMetadata
}