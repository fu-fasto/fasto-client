import React, { useState } from "react"
import {
  Card,
  CardBody,
  Input,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle
} from "reactstrap"
import { AgGridReact } from "ag-grid-react"
import { ContextLayout } from "../../utility/context/Layout"
import { ChevronDown } from "react-feather"

import "../../assets/scss/plugins/tables/_agGridStyleOverride.scss"

const FastTable = ({ rowData, columnDefs, actions = [] }) => {
  const [query, setQuery] = useState("")
  const [metadata, setMetadata] = useState({
    gridApi: null,
    paginationPageSize: 20,
    currentPageSize: "",
    getPageSize: "",
    defaultColDef: {
      sortable: true,
      resizable: true,
      filterParams: { newRowsAction: "keep" },
      suppressSizeToFit: true
      // filter: true
    },
    columnDefs: []
  })

  const onGridReady = params => {
    const newColumnDefs = columnDefs.map(c => ({
      // filter: true,
      // flex: 1,
      // comparator: () => 0,
      ...c
    }))

    setMetadata({
      ...metadata,
      gridApi: params.api,
      gridColumnApi: params.columnApi,
      columnDefs: newColumnDefs,
      currentPageSize: params.api.paginationGetCurrentPage() + 1,
      getPageSize: params.api.paginationGetPageSize(),
      totalPages: params.api.paginationGetTotalPages()
    })
  }

  const updateSearchQuery = val => {
    metadata.gridApi.setQuickFilter(val)
    setQuery(val)
  }

  const filterSize = val => {
    if (metadata.gridApi) {
      metadata.gridApi.paginationSetPageSize(Number(val))
      setMetadata({
        ...metadata,
        currentPageSize: val,
        getPageSize: val
      })
    }
  }

  return (
    <Card className="overflow-hidden agGrid-card ">
      <CardBody className="py-0">
        {rowData === null ? null : (
          <div className="ag-theme-material w-100 my-2 ag-grid-table">
            <div className="d-flex flex-wrap justify-content-between align-items-center">
              <div className="mb-1">
                <UncontrolledDropdown className="p-1 ag-dropdown">
                  <DropdownToggle tag="div">
                    {metadata.gridApi
                      ? metadata.currentPageSize
                      : "" * metadata.getPageSize -
                        (metadata.getPageSize - 1)}{" "}
                    -{" "}
                    {rowData.length -
                      metadata.currentPageSize * metadata.getPageSize >
                    0
                      ? metadata.currentPageSize * metadata.getPageSize
                      : rowData.length}{" "}
                    of {rowData.length}
                    <ChevronDown className="ml-50" size={15} />
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem tag="div" onClick={() => filterSize(20)}>
                      20
                    </DropdownItem>
                    <DropdownItem tag="div" onClick={() => filterSize(50)}>
                      50
                    </DropdownItem>
                    <DropdownItem tag="div" onClick={() => filterSize(100)}>
                      100
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </div>
              <div className="d-flex flex-wrap justify-content-between mb-1">
                <div className="table-input mr-1">
                  <Input
                    color="primary"
                    placeholder="Search..."
                    onChange={e => updateSearchQuery(e.target.value)}
                    value={query}
                  />
                </div>
                <div className="mr-1" hidden={!actions.length}>
                  <UncontrolledDropdown>
                    <DropdownToggle className="px-2 py-75" color="primary">
                      Actions
                      <ChevronDown className="ml-50" size={15} />
                    </DropdownToggle>
                    <DropdownMenu right>
                      {actions.map(action => (
                        <DropdownItem
                          key={Math.random()}
                          onClick={action.onClick}
                          style={{ width: "-webkit-fill-available" }}
                        >
                          <span className="align-middle">{action.text}</span>
                        </DropdownItem>
                      ))}
                      {/*                       
                      <DropdownItem
                        onClick={() => metadata.gridApi.exportDataAsCsv()}
                        style={{ width: "-webkit-fill-available" }}
                      >
                        <Printer size={15} />
                        <span className="align-middle">Export CSV</span>
                      </DropdownItem> 
                      */}
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </div>
              </div>
            </div>
            <ContextLayout.Consumer>
              {context => (
                <AgGridReact
                  // gridOptions={{}}
                  // rowSelection="multiple"
                  defaultColDef={metadata.defaultColDef}
                  columnDefs={metadata.columnDefs}
                  rowData={rowData}
                  onGridReady={onGridReady}
                  colResizeDefault={"shift"}
                  animateRows={true}
                  // floatingFilter={true}
                  suppressAutoSize
                  pagination={true}
                  paginationPageSize={metadata.paginationPageSize}
                  pivotPanelShow="always"
                />
              )}
            </ContextLayout.Consumer>
          </div>
        )}
      </CardBody>
    </Card>
  )
}

export default FastTable
