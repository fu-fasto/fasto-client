import React, { useEffect } from "react"
import { useSelector, useDispatch } from "react-redux"
import { getRoles } from "@src/redux/actions/userActions"
import { getReservations, getVisits } from "@src/redux/actions/rvActions"
import { initConnection } from "../../services/signalRService"
import { getAllPaymentTransactions } from "../../redux/actions/paymentTransactionActions"

function initReservations(dispatch, user) {
  const findQuery = {}

  if (user.storeId) {
    findQuery.storeId = user.storeId
  } else if (user.role === "CUSTOMER") {
    findQuery.customerId = user.id
  }

  dispatch(getReservations(findQuery))
}

function initRoles(dispatch, user) {
  dispatch(getRoles(user))
}

function initVisits(dispatch, user) {
  const findQuery = {}

  if (user.storeId) {
    findQuery.storeId = user.storeId
  } else if (user.role === "CUSTOMER") {
    findQuery.customerId = user.id
  }
  findQuery.isActivated = true;

  dispatch(getVisits(findQuery))
}

function initPaymentTransactions(dispatch, user) {
  const findQuery = {}
  if (user.role === "STORE.ADMIN") {
    findQuery.storeId = user.storeId
  } else if (user.role === "CUSTOMER") {
    findQuery.customerId = user.id
  }
  dispatch(getAllPaymentTransactions(findQuery))
}

const UserSetup = () => {
  const dispatch = useDispatch()
  const user = useSelector(x => x.auth)

  useEffect(() => {
    initConnection(dispatch)
  }, [dispatch])

  useEffect(() => {
    if (!user) return

    initReservations(dispatch, user)
    initRoles(dispatch, user)
    initVisits(dispatch, user)
    initPaymentTransactions(dispatch, user)
  }, [dispatch, user])

  return <div />
}

export default UserSetup
