import React, { useState, useEffect } from "react"
import {
  Row,
  Col,
  Card,
  CardBody,
  FormGroup,
  Button,
  Input,
  Form
} from "reactstrap"
import Flatpickr from "react-flatpickr"
import Select from "react-select"
import * as Yup from "yup"
import "flatpickr/dist/themes/light.css"
import "../../assets/scss/plugins/forms/flatpickr/flatpickr.scss"
// import NumericInput from "react-numeric-input"
// import { mobileStyle } from "./InputStyles"

const FastForm = ({
  validationSchema = Yup.object().shape({}),
  definitions = [],
  actions = [],
  onSubmit = () => {}
}) => {
  const [values, setValues] = useState({})
  const [finalDefinitions, setFinalDefinitions] = useState([])
  const [errors, setErrors] = useState({})
  

  const handleSubmit = e => {
    e.preventDefault()

    validationSchema
      .validate(values, { abortEarly: false })
      .then(() => {
        setErrors({})
        onSubmit(values)
      })
      .catch(invalid => {
        const errors = invalid.inner.reduce((object, err) => {
          object[err.path] = err.type === "typeError" ? "Required" : err.message
          return object
        }, {})
        setErrors(errors)
      })
  }

  const handleChangeValues = (field, value) => {
    setValues({
      ...values,
      [field]: value
    })
  }

  useEffect(() => {
    const defaultValue = {
      "text": "",
      "number": 0,
      "date": new Date(),
      "datetime": new Date(),
      "time": new Date()
    }
    const initialValues =
      definitions.reduce((object, definition) => {
        object[definition.field] = definition.value ?? definition.defaultValue ?? defaultValue[definition.type]
        return object
      }, {}) || {}
    setValues(initialValues)
    setFinalDefinitions(definitions)
  }, [definitions])

  return (
    <Card>
      {/* <CardHeader>
        <CardTitle>{formTitle}</CardTitle>
      </CardHeader> */}
      <CardBody>
        <Form onSubmit={handleSubmit}>
          <Row>
            {finalDefinitions.map(definition => {
              let input
              switch (definition.type) {
                // case "number":
                //   input = (
                //     <NumericInput
                //       min={0}
                //       max={100}
                //       value={values[definition.field]}
                //       mobile
                //       onChange={value =>
                //         handleChangeValues(definition.field, value)
                //       }
                //       style={mobileStyle}
                //     />
                //   )
                //   break
                case "select":
                  input = (
                    <Select
                      // key={Math.random()}
                      key={definition.field}
                      classNamePrefix="select"
                      value={definition.selectOptions?.find(
                        x => x.value === values[definition.field]
                      )}
                      name="color"
                      options={definition.selectOptions}
                      className={`React ${
                        errors[definition.field] ? "is-invalid" : null
                      }`}
                      onChange={({ value }) => {
                        handleChangeValues(definition.field, value)
                      }}
                    />
                  )
                  break

                case "time":
                  input = (
                    <Flatpickr
                      key={Math.random()}
                      value={values[definition.field]}
                      disabled={definition.disabled}
                      options={{
                        enableTime: true,
                        noCalendar: true,
                        dateFormat: "H:i"
                      }}
                      onChange={dates =>
                        handleChangeValues(definition.field, dates[0])
                      }
                      className={`form-control ${
                        errors[definition.field] ? "is-invalid" : null
                      }`}
                    />
                  )
                  break

                case "date":
                  input = (
                    <Flatpickr
                      key={Math.random()}
                      value={values[definition.field]}
                      disabled={definition.disabled}
                      onChange={dates =>
                        handleChangeValues(definition.field, dates[0])
                      }
                      className={`form-control ${
                        errors[definition.field] ? "is-invalid" : null
                      }`}
                    />
                  )
                  break

                case "datetime":
                  input = (
                    <Flatpickr
                      key={Math.random()}
                      data-enable-time
                      value={values[definition.field]}
                      disabled={definition.disabled}
                      onChange={dates =>
                        handleChangeValues(definition.field, dates[0])
                      }
                      className={`form-control ${
                        errors[definition.field] ? "is-invalid" : null
                      }`}
                    />
                  )
                  break
                default:
                  input = (
                    <Input
                      type={definition.type}
                      name={definition.field}
                      id={definition.field}
                      disabled={definition.disabled}
                      value={values[definition.field]}
                      onChange={e =>
                        handleChangeValues(definition.field, e.target.value)
                      }
                      className={`form-control ${
                        errors[definition.field] ? "is-invalid" : null
                      }`}
                    />
                  )
              }

              return (
                <Col
                  key={definition.field}
                  hidden={definition?.hidden}
                  sm={definition?.sm ?? 12}
                  md={definition?.md ?? 12}
                  lg={definition?.lg ?? 12}
                >
                  <FormGroup
                    className={errors[definition.field] ? "mb-3" : null}
                    key={definition.field}
                  >
                    <h6 className="my-1 text-bold-600">
                      {definition.headerName}
                    </h6>
                    {input}
                    {errors[definition.field] ? (
                      <div
                        className="invalid-tooltip"
                        style={{ left: "unset" }}
                      >
                        {errors[definition.field]}
                      </div>
                    ) : null}
                  </FormGroup>
                </Col>
              )
            })}
          </Row>

          {actions.map(
            ({ text, type = "reset", color = "primary", onClick }) => (
              <Button.Ripple
                className="mr-1"
                key={text}
                color={color}
                onClick={onClick}
                type={type}
              >
                {text}
              </Button.Ripple>
            )
          )}
        </Form>
      </CardBody>
    </Card>
  )
}

export default FastForm
