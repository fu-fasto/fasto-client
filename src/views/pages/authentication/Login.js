/* eslint-disable no-unused-vars */
import React, { useState } from "react"
import { useDispatch } from "react-redux"
import {
  Button,
  Card,
  CardBody,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label
} from "reactstrap"
import { Mail, Lock, Check, Facebook, Twitter, GitHub } from "react-feather"
import Checkbox from "@src/components/@vuexy/checkbox/CheckboxesVuexy"
import googleSvg from "@src/assets/img/svg/google.svg"
import JwtService from "@src/utility/auth/jwt/jwtService"
import { login } from "@src/redux/actions/authActions"
import loginImg from "@src/assets/img/pages/login.png"
import { useHistory } from "react-router-dom"
import "@src/assets/scss/pages/authentication.scss"
import qs from "qs"

const Login = props => {
  const { redirect } = qs.parse(props.location.search, {
    ignoreQueryPrefix: true
  })
  const history = useHistory()
  const dispatch = useDispatch()
  const [state, setState] = useState({
    email: "",
    password: ""
  })

  const handleSubmit = e => {
    e.preventDefault()

    JwtService.login({
      email: state.email,
      password: state.password
    })
      .then(response => {
        dispatch(login(response))

        // Push user to suitable by role
        if (redirect) {
          // setTimeout(() => {
            history.push(redirect)
          // }, 1000)
        } else {
          history.push("/")
        }
      })
      .catch(err => {})
  }
  return (
    <Row className="m-0 justify-content-center">
      <Col
        sm="8"
        xl="7"
        lg="10"
        md="8"
        className="d-flex justify-content-center"
      >
        <Card className="bg-authentication login-card rounded-0 mb-0 w-100">
          <Row className="m-0">
            <Col
              lg="6"
              className="d-lg-block d-none text-center align-self-center px-1 py-0"
            >
              <img src={loginImg} alt="loginImg" />
            </Col>
            <Col lg="6" md="12" className="p-0">
              <Card className="rounded-0 mb-0 px-2">
                <CardBody>
                  <h4>Login</h4>
                  <p>Welcome back, please login to your account.</p>
                  <Form
                    onKeyDown={e => {
                      if (e.keyCode === 27 || e.keyCode === 13) {
                        handleSubmit(e)
                      }
                    }}
                    onSubmit={e => e.preventDefault()}
                  >
                    <FormGroup className="form-label-group position-relative has-icon-left">
                      <Input
                        type="email"
                        placeholder="Email"
                        value={state.email}
                        onChange={e =>
                          setState({ ...state, email: e.target.value })
                        }
                      />
                      <div className="form-control-position">
                        <Mail size={15} />
                      </div>
                      <Label>Email</Label>
                    </FormGroup>
                    <FormGroup className="form-label-group position-relative has-icon-left">
                      <Input
                        type="password"
                        placeholder="Password"
                        value={state.password}
                        onChange={e =>
                          setState({ ...state, password: e.target.value })
                        }
                      />
                      <div className="form-control-position">
                        <Lock size={15} />
                      </div>
                      <Label>Password</Label>
                    </FormGroup>
                    <FormGroup className="d-flex justify-content-between align-items-center">
                      <Checkbox
                        color="primary"
                        icon={<Check className="vx-icon" size={16} />}
                        label="Remember me"
                      />
                      <div className="float-right">Forgot Password?</div>
                    </FormGroup>
                    <div className="d-flex justify-content-between">
                      <Button
                        className="mr-1 mb-1 w-100"
                        color="primary"
                        outline
                      >
                        Register
                      </Button>
                      <Button
                        className="mb-1 w-100"
                        color="primary"
                        type="submit"
                        onClick={handleSubmit}
                      >
                        Login
                      </Button>
                    </div>
                  </Form>
                </CardBody>
                <div className="auth-footer">
                  <div className="divider">
                    <div className="divider-text">OR</div>
                  </div>
                  <div className="footer-btn">
                    {/* <Button.Ripple className="btn-facebook" color="">
                      <Facebook size={14} />
                    </Button.Ripple>
                    <Button.Ripple className="btn-twitter" color="">
                      <Twitter size={14} stroke="white" />
                    </Button.Ripple>
                    <Button.Ripple className="btn-google" color="">
                      <img
                        src={googleSvg}
                        alt="google"
                        height="15"
                        width="15"
                      />
                    </Button.Ripple>
                    <Button.Ripple className="btn-github" color="">
                      <GitHub size={14} stroke="white" />
                    </Button.Ripple> */}
                    <Button
                      className="w-100"
                      color="info"
                      onClick={() => history.push("/")}
                    >
                      Go Home Anyway
                    </Button>
                  </div>
                </div>
              </Card>
            </Col>
          </Row>
        </Card>
      </Col>
    </Row>
  )
}
export default Login
