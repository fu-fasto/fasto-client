import React, { useEffect } from "react"
import { useDispatch } from "react-redux"
import { logout } from "@src/redux/actions/authActions"
import { history } from "@src/history"

const Logout = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(logout())
    history.push("/login")
  }, [dispatch])

  return <div />
}
export default Logout
