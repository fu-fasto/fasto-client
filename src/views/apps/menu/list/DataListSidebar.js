import React, { useEffect, useState } from "react"
import { Label, Input, FormGroup, Button } from "reactstrap"
import { X } from "react-feather"
import PerfectScrollbar from "react-perfect-scrollbar"
import classnames from "classnames"
import { useDispatch } from "react-redux"

function DataListSidebar(props) {
  const {
    data,
    show,
    handleSidebar,
    updateData,
    addData,
    getData,
    dataParams
  } = props
  const [id, setId] = useState("")
  const [name, setName] = useState("")
  const [price, setPrice] = useState("")
  const [img, setImg] = useState("")
  const dispatch = useDispatch()

  let addNew = false

  useEffect(() => {
    if (addNew) {
      setId("")
      setName("")
      setPrice("")
      setImg("")
    }
    if (data !== null) {
      setId(data.id)
      setName(data.name)
      setPrice(data.price)
      setImg(data.img)
    }
  }, [data, addNew])

  const handleSubmit = obj => {
    if (data !== null) {
      dispatch(updateData(obj))
    } else {
      addNew = true
      dispatch(addData(obj))
    }
    let params = Object.keys(dataParams).length
      ? dataParams
      : { page: 1, perPage: 4 }
    handleSidebar(false, true)
    dispatch(getData(params))
  }

  return (
    <div
      className={classnames("data-list-sidebar", {
        show: show
      })}
    >
      <div className="data-list-sidebar-header mt-2 px-2 d-flex justify-content-between">
        <h4>{data !== null ? "UPDATE DATA" : "ADD NEW DATA"}</h4>
        <X size={20} onClick={() => handleSidebar(false, true)} />
      </div>
      <PerfectScrollbar
        className="data-list-fields px-2 mt-3"
        options={{ wheelPropagation: false }}
      >
        {props.thumbView && img.length ? (
          <FormGroup className="text-center">
            <img className="img-fluid" src={img} alt={name} />
            <div className="d-flex flex-wrap justify-content-between mt-2">
              <label
                className="btn btn-flat-primary"
                htmlFor="update-image"
                color="primary"
              >
                Upload Image
                <input
                  type="file"
                  id="update-image"
                  hidden
                  onChange={e => setImg(URL.createObjectURL(e.target.files[0]))}
                />
              </label>
              <Button color="flat-danger" onClick={() => setImg("")}>
                Remove Image
              </Button>
            </div>
          </FormGroup>
        ) : null}
        <FormGroup>
          <Label for="data-name">Name</Label>
          <Input
            type="text"
            value={name}
            placeholder="Apple IphoneX"
            onChange={e => setName(e.target.value)}
            id="data-name"
          />
        </FormGroup>
        <FormGroup>
          <Label for="data-price">Price</Label>
          <Input
            type="number"
            value={price}
            onChange={e => setPrice(e.target.value)}
            id="data-price"
            placeholder="69.99"
          />
        </FormGroup>
        {props.thumbView && img.length <= 0 ? (
          <label
            className="btn btn-primary"
            htmlFor="upload-image"
            color="primary"
          >
            Upload Image
            <input
              type="file"
              id="upload-image"
              hidden
              onChange={e => setImg(URL.createObjectURL(e.target.files[0]))}
            />
          </label>
        ) : null}
      </PerfectScrollbar>
      <div className="data-list-sidebar-footer px-2 d-flex justify-content-start align-items-center mt-1">
        <Button
          color="primary"
          onClick={() =>
            handleSubmit({
              id,
              name,
              price,
              img
            })
          }
        >
          {data !== null ? "Update" : "Submit"}
        </Button>
        <Button
          className="ml-1"
          color="danger"
          outline
          onClick={() => handleSidebar(false, true)}
        >
          Cancel
        </Button>
      </div>
    </div>
  )
}

export default DataListSidebar
