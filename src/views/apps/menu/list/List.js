import React from "react"
import { Row, Col } from "reactstrap"
import Breadcrumbs from "../../../../components/@vuexy/breadCrumbs/BreadCrumb"
import ListConfig from "./DataListConfig"
import queryString from "query-string"

function List(props) {
  const { location } = props

  return (
    <div>
      <React.Fragment>
        <Breadcrumbs
          breadCrumbTitle="Menu Management"
          breadCrumbParent="Home"
          breadCrumbActive="Menu Management"
        />
        <Row>
          <Col sm="12">
            <ListConfig
              thumbView={true}
              parsedFilter={queryString.parse(location.search)}
            />
          </Col>
        </Row>
      </React.Fragment>
    </div>
  )
}

export default List
