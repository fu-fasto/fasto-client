import React, { useState, useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import {
  Button,
  // Progress,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  Input
} from "reactstrap"
import SweetAlert from "react-bootstrap-sweetalert"
import DataTable from "react-data-table-component"
import classnames from "classnames"
import ReactPaginate from "react-paginate"
import { history } from "../../../../history"
import {
  Edit,
  Trash,
  ChevronDown,
  Plus,
  Check,
  ChevronLeft,
  ChevronRight
} from "react-feather"
import {
  getData,
  getInitialData,
  deleteData,
  updateData,
  addData,
  filterData
} from "../../../../redux/actions/data-list"
import Sidebar from "./DataListSidebar"
// import Chip from "../../../../components/@vuexy/chips/ChipComponent";
import Checkbox from "../../../../components/@vuexy/checkbox/CheckboxesVuexy"

import "../../../../assets/scss/plugins/extensions/react-paginate.scss"
import "../../../../assets/scss/pages/data-list.scss"

const selectedStyle = {
  rows: {
    selectedHighlighStyle: {
      backgroundColor: "rgba(115,103,240,.05)",
      color: "#7367F0 !important",
      boxShadow: "0 0 1px 0 #7367F0 !important",
      "&:hover": {
        transform: "translateY(0px) !important"
      }
    }
  }
}

function ActionsComponent(props) {
  const { currentData, row, handleAlert, handleDeletedRow } = props
  return (
    <div className="data-list-action">
      <Edit
        className="cursor-pointer mr-1"
        size={20}
        onClick={() => {
          return currentData(row)
        }}
      />
      <Trash
        className="cursor-pointer"
        size={20}
        onClick={() => {
          handleAlert("delete", true)
          handleDeletedRow(row)
        }}
      />
    </div>
  )
}

function CustomHeader(props) {
  const {
    handleFilter,
    handleRowsPerPage,
    handleDeleteRows,
    selectedRows,
    handleSidebar,
    index,
    total
  } = props
  return (
    <div className="data-list-header d-flex justify-content-between flex-wrap">
      <div className="actions-left d-flex flex-wrap">
        <UncontrolledDropdown className="data-list-dropdown mr-1">
          <DropdownToggle className="p-1" color="primary">
            <span className="align-middle mr-1">Actions</span>
            <ChevronDown size={15} />
          </DropdownToggle>
          <DropdownMenu tag="div" right>
            <DropdownItem
              tag="a"
              onClick={() => handleDeleteRows(selectedRows)}
            >
              Delete
            </DropdownItem>
            <DropdownItem tag="a">Archive</DropdownItem>
            <DropdownItem tag="a">Print</DropdownItem>
            <DropdownItem tag="a">Export</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <Button
          className="add-new-btn"
          color="primary"
          onClick={() => handleSidebar(true, true)}
          outline
        >
          <Plus size={15} />
          <span className="align-middle">Add New</span>
        </Button>
      </div>
      <div className="actions-right d-flex flex-wrap mt-sm-0 mt-2">
        <UncontrolledDropdown className="data-list-rows-dropdown mr-1 d-md-block d-none">
          <DropdownToggle color="" className="sort-dropdown">
            <span className="align-middle mx-50">
              {`${index[0]} - ${index[1]} of ${total}`}
            </span>
            <ChevronDown size={15} />
          </DropdownToggle>
          <DropdownMenu tag="div" right>
            <DropdownItem tag="a" onClick={() => handleRowsPerPage(4)}>
              4
            </DropdownItem>
            <DropdownItem tag="a" onClick={() => handleRowsPerPage(10)}>
              10
            </DropdownItem>
            <DropdownItem tag="a" onClick={() => handleRowsPerPage(15)}>
              15
            </DropdownItem>
            <DropdownItem tag="a" onClick={() => handleRowsPerPage(20)}>
              20
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <div className="filter-section">
          <Input type="text" onChange={e => handleFilter(e)} />
        </div>
      </div>
    </div>
  )
}

function DataListConfig(props) {
  const { parsedFilter } = props
  const columnDef = [
    {
      name: "Image",
      selector: "img",
      minWidth: "220px",
      cell: row => <img src={row.img} height="100" alt={row.name} />
    },
    {
      name: "Name",
      selector: "name",
      sortable: true,
      minWidth: "300px",
      cell: row => (
        <p title={row.name} className="text-truncate text-bold-500 mb-0">
          {row.name}
        </p>
      )
    },
    // {
    //   name: "Category",
    //   selector: "category",
    //   sortable: true,
    // },
    // {
    //   name: "Popularity",
    //   selector: "popularity",
    //   sortable: true,
    //   cell: (row) => (
    //     <Progress
    //       className="w-100 mb-0"
    //       color={row.popularity.color}
    //       value={row.popularity.popValue}
    //     />
    //   ),
    // },
    // {
    //   name: "Order Status",
    //   selector: "order_status",
    //   sortable: true,
    //   cell: (row) => (
    //     <Chip
    //       className="m-0"
    //       color={chipColors[row.order_status]}
    //       text={row.order_status}
    //     />
    //   ),
    // },
    {
      name: "Price",
      selector: "price",
      sortable: true,
      cell: row => `$${row.price}`
    },
    {
      name: "Actions",
      sortable: true,
      cell: row => (
        <ActionsComponent
          row={row}
          getData={getData}
          parsedFilter={parsedFilter}
          currentData={handleCurrentData}
          handleAlert={handleAlert}
          handleDeletedRow={handleDeletedRow}
        />
      )
    }
  ]
  const [deleteAlert, setDeleteAlert] = useState(false)
  const [confirmAlert, setConfirmAlert] = useState(false)
  const [cancelAlert, setCancelAlert] = useState(false)
  const [setCurrentPage] = useState(0)
  const [columns] = useState(columnDef)
  const [value, setValue] = useState("")
  const [rowsPerPage, setRowsPerPage] = useState(4)
  const [sidebar, setSidebar] = useState(false)
  const [currentData, setCurrentData] = useState(null)
  const [selected, setSelected] = useState([])
  const [deletedRow, setDeletedRow] = useState(null)
  const [addNew, setAddNew] = useState("")

  const dispatch = useDispatch()
  const data = useSelector(state => state.dataList.data)
  const allData = useSelector(state => state.dataList.allData)
  //const filteredData = useSelector(state => state.dataList.filteredData)
  const totalPages = useSelector(state => state.dataList.totalPages)
  const totalRecords = useSelector(state => state.dataList.totalRecords)
  const sortIndex = useSelector(state => state.dataList.sortIndex)

  useEffect(() => {
    dispatch(getData(parsedFilter))
    dispatch(getInitialData())
  }, [dispatch, parsedFilter])

  const handleAlert = (state, value) => {
    switch (state) {
      case "delete":
        setDeleteAlert(value)
        return
      case "confirm":
        setConfirmAlert(value)
        return
      case "cancel":
        setCancelAlert(value)
        return
      default:
        return
    }
  }

  const handleFilter = e => {
    setValue(e.target.value)
    dispatch(filterData(e.target.value))
  }

  const handleRowsPerPage = value => {
    let page = parsedFilter.page !== undefined ? parsedFilter.page : 1
    history.push(`/menu-management/list-view?page=${page}&perPage=${value}`)
    setRowsPerPage(value)
    dispatch(getData({ page: parsedFilter.page, perPage: value }))
  }

  const handleSidebar = (boolean, addNew = false) => {
    setSidebar(boolean)
    if (addNew === true) {
      setCurrentData(null)
      setAddNew(true)
    }
  }

  const handleDeletedRow = value => {
    setDeletedRow(value)
  }

  const handleDelete = row => {
    dispatch(deleteData(row))
    dispatch(getData(parsedFilter))
    if (data.length - 1 === 0) {
      let urlPrefix = "/menu-management/"
      history.push(
        `${urlPrefix}list-view?page=${parseInt(
          parsedFilter.page - 1
        )}&perPage=${parsedFilter.perPage}`
      )
    }
  }

  const handleDeleteRows = rows => {
    rows.map(row => handleDelete(row))
  }

  const handleCurrentData = obj => {
    setCurrentData(obj)
    handleSidebar(true)
  }

  const handlePagination = page => {
    let perPage = parsedFilter.perPage !== undefined ? parsedFilter.perPage : 4
    let urlPrefix = "/menu-management/"
    history.push(
      `${urlPrefix}list-view?page=${page.selected + 1}&perPage=${perPage}`
    )
    dispatch(getData({ page: page.selected + 1, perPage: perPage }))
    setCurrentPage(page.selected)
  }

  return (
    <div className={"data-list thumb-view"}>
      <DataTable
        columns={columns}
        data={value.length ? allData : data}
        pagination
        paginationServer
        paginationComponent={() => (
          <ReactPaginate
            previousLabel={<ChevronLeft size={15} />}
            nextLabel={<ChevronRight size={15} />}
            breakLabel="..."
            breakClassName="break-me"
            pageCount={totalPages}
            containerClassName="vx-pagination separated-pagination pagination-end pagination-sm mb-0 mt-2"
            activeClassName="active"
            forcePage={parsedFilter.page ? parseInt(parsedFilter.page - 1) : 0}
            onPageChange={page => handlePagination(page)}
          />
        )}
        noHeader
        subHeader
        selectableRows
        responsive
        pointerOnHover
        selectableRowsHighlight
        onSelectedRowsChange={data => setSelected(data.selectedRows)}
        customStyles={selectedStyle}
        subHeaderComponent={
          <CustomHeader
            handleSidebar={handleSidebar}
            handleFilter={handleFilter}
            handleRowsPerPage={handleRowsPerPage}
            rowsPerPage={rowsPerPage}
            total={totalRecords}
            index={sortIndex}
            handleDeleteRows={handleDeleteRows}
            selectedRows={selected}
          />
        }
        sortIcon={<ChevronDown />}
        selectableRowsComponent={Checkbox}
        selectableRowsComponentProps={{
          color: "primary",
          icon: <Check className="vx-icon" size={12} />,
          label: "",
          size: "sm"
        }}
      />
      <Sidebar
        show={sidebar}
        data={currentData}
        updateData={updateData}
        addData={addData}
        handleSidebar={handleSidebar}
        thumbView={true}
        getData={getData}
        dataParams={parsedFilter}
        addNew={addNew}
      />
      <div
        className={classnames("data-list-overlay", {
          show: sidebar
        })}
        onClick={() => handleSidebar(false, true)}
      />
      <SweetAlert
        title="Are you sure?"
        warning
        show={deleteAlert}
        showCancel
        reverseButtons
        cancelBtnBsStyle="danger"
        confirmBtnText="Yes, delete it!"
        cancelBtnText="Cancel"
        onConfirm={() => {
          handleAlert("confirm", true)
          handleDelete(deletedRow)
        }}
        onCancel={() => {
          handleAlert("basicAlert", false)
          handleAlert("cancel", true)
        }}
      >
        You won't be able to revert this!
      </SweetAlert>

      <SweetAlert
        success
        title="Deleted!"
        confirmBtnBsStyle="success"
        show={confirmAlert}
        onConfirm={() => {
          handleAlert("delete", false)
          handleAlert("confirm", false)
        }}
      >
        <p className="sweet-alert-text">Item has been deleted.</p>
      </SweetAlert>

      <SweetAlert
        error
        title="Cancelled"
        confirmBtnBsStyle="success"
        show={cancelAlert}
        onConfirm={() => {
          handleAlert("delete", false)
          handleAlert("cancel", false)
        }}
      >
        <p className="sweet-alert-text">Your imaginary file is safe :)</p>
      </SweetAlert>
    </div>
  )
}

export default DataListConfig
