import React, { useState } from "react"
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardSubtitle,
  Col,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row
} from "reactstrap"
import "../../../assets/scss/pages/menu-view.scss"
import NumericInput from "react-numeric-input"
import * as Yup from "yup"
import { toast, ToastContainer } from "react-toastify"
import "react-toastify/dist/ReactToastify.css"
import "../../../assets/scss/plugins/extensions/toastr.scss"
import { useDispatch, useSelector } from "react-redux"
import { makeOrder } from "../../../redux/actions/orderActions"

const formatCurrency = money => {
  return String(money).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
}

const mobileStyle = {
  wrap: {
    background: "#E2E2E2",
    fontSize: 14
  },
  "input.mobile": {
    color: "5f5f5f",
    padding: "0",
    border: 0,
    display: "block",
    fontWeight: 400,
    backgroundColor: "#f8f8f8",
    height: "26px"
  },
  "input:focus": {
    outline: "none"
  },
  arrowUp: {
    borderBottomColor: "#fff"
  },
  arrowDown: {
    borderTopColor: "#fff"
  },
  plus: {
    background: "white"
  },
  minus: {
    background: "white"
  },
  "btnUp.mobile": {
    background: "#7367F0",
    borderRadius: "5px",
    height: "22px",
    width: "22px",
    top: "2px",
    cursor: "pointer"
  },
  "btnDown.mobile": {
    background: "#7367F0",
    borderRadius: "5px",
    height: "22px",
    width: "22px",
    top: "2px",
    cursor: "pointer"
  }
}

function MenuView({ visit, storeItems }) {
  const [showItemDetail, setShowItemDetail] = useState(false)
  const [selectedItem, setSelectedItem] = useState({})
  const dispatch = useDispatch()
  const user = useSelector(x => x.auth)

  const validationSchema = Yup.object().shape({
    quantity: Yup.number()
      .integer("The quantity must be a integer number")
      .positive("The quantity must be a positive number")
      .moreThan(0, "The quantity must be greater than 0")
      .required("Please provide the quantity")
  })

  const toggleItemDetail = () => {
    setShowItemDetail(prevState => !prevState)
  }

  const handleCardClick = item => {
    toggleItemDetail()
    setSelectedItem({ ...item, quantity: 1, note: "" })
  }

  const submitOrder = async data => {
    await dispatch(
      makeOrder({
        quantity: data.quantity,
        note: data.note,
        storeId: visit?.storeId,
        customerId: user.id,
        orderTime: new Date().toJSON(),
        visitId: visit?.id,
        itemId: data.id,
        item: data
      })
    )
  }

  return (
    <React.Fragment>
      <Row>
        {storeItems.map(item => (
          <Col xs="6" md="3" className="item-card py-2" key={item.id}>
            <div
              style={{ cursor: "pointer" }}
              onClick={() => handleCardClick(item)}
            >
              <Card className="h-100">
                <CardImg top src={item.imageUrl} />
                <CardBody>
                  <CardSubtitle
                    style={{
                      "text-overflow": "ellipsis",
                      "white-space": "nowrap",
                      overflow: "hidden"
                    }}
                  >
                    <strong>{item.name}</strong>
                  </CardSubtitle>
                  <CardSubtitle>{formatCurrency(item.price)}đ</CardSubtitle>
                </CardBody>
              </Card>
            </div>
          </Col>
        ))}
      </Row>
      <Modal
        isOpen={showItemDetail}
        toggle={toggleItemDetail}
        scrollable={true}
        className="modal-dialog-centered"
      >
        <ModalHeader>
          <div>{selectedItem?.name}</div>
          <div>{formatCurrency(selectedItem.price)}đ</div>
        </ModalHeader>
        <ModalBody>
          <Row>
            <Col xs={{ size: 12, order: 1 }} md={{ size: 4, order: 2 }}>
              <img
                style={{ borderRadius: "5px" }}
                className="pb-1 pb-md-0"
                src={selectedItem?.imageUrl}
                width="100%"
                alt="meal"
              />
            </Col>
            <Col
              xs={{ size: 12, order: 2 }}
              md={{ size: 8, order: 1 }}
              className="d-flex align-items-baseline"
            >
              <h5 className="text-bold-400 pr-1 primary">Note</h5>
              <Input
                type="textarea"
                placeholder="Take note for store?"
                onChange={event => {
                  setSelectedItem({ ...selectedItem, note: event.target.value })
                }}
              />
            </Col>
          </Row>
        </ModalBody>
        <ModalFooter className="d-flex justify-content-between">
          <NumericInput
            mobile
            min={1}
            defaultValue={1}
            style={mobileStyle}
            onChange={data => {
              setSelectedItem({ ...selectedItem, quantity: data })
            }}
          />
          <Button
            color="primary"
            onClick={() => {
              validationSchema
                .validate(selectedItem, { abortEarly: false })
                .then(selectedItem => {
                  toast.success(`${selectedItem.name} order successfully`)
                  submitOrder(selectedItem)
                  setSelectedItem({})
                  toggleItemDetail()
                })
                .catch(({ message }) => {
                  toast.error(message)
                })
            }}
          >
            Order
          </Button>
        </ModalFooter>
      </Modal>
      <ToastContainer />
    </React.Fragment>
  )
}

export default MenuView
