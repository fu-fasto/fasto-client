import React from "react"
import FastForm from "../../../components/@fasto/FastForm"
import * as Yup from "yup"
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { makeOrder } from "../../../redux/actions/orderActions"

const schema = Yup.object().shape({
  peopleNumber: Yup.number().moreThan(0, "Must be greater than zero!"),
  expectedTime: Yup.date().required("Required")
})

const OrderCreating = props => {
  const dispatch = useDispatch()
  const history = useHistory()
  const { id } = props.match.params
  const user = useSelector(x => x.auth)

  const definitions = [
    {
      headerName: "Items",
      field: "itemId",
      type: "text"
    },
    {
      headerName: "Quantity",
      field: "orderQuantity",
      type: "number"
    },
    {
      headerName: "Note",
      field: "note",
      type: "text"
    }
  ]
  const actions = [
    {
      text: "Make order",
      type: "submit"
    },
    {
      text: "Back",
      color: "warning",
      onClick: () => history.goBack()
    }
  ]

  const handleSubmit = async values => {
    await dispatch(
      makeOrder({
        ...values,
        storeId: "302f5e00-55de-433f-8045-d83654f77ebf",
        visitId: id,
        customerId: user.id,
        processing: true
      })
    )
    history.goBack()
  }

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        validationSchema={schema}
        actions={actions}
      />
    </React.Fragment>
  )
}
export default OrderCreating
