import React, { useEffect, useState } from "react"
import moment from "moment"
import FastTable from "@fasto/FastTable"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { Delete, Eye } from "react-feather"
import { updateOrder } from "../../../redux/actions/orderActions"
import itemService from "../../../services/itemService"
import { Nav, NavItem, NavLink, Spinner, TabContent, TabPane } from "reactstrap"
import Checkout from "../checkout/Checkout"
import classnames from "classnames"
import MenuView from "./MenuView"
import qs from "qs"

const OrderList = props => {
  const history = useHistory()
  const [active, setActive] = useState("1")
  const [ordersOfCurrentVisit, setOrdersOfCurrentVisit] = useState([])
  const [currentVisit, setCurrentVisit] = useState({})
  const [storeItems, setStoreItems] = useState([])
  const user = useSelector(x => x.auth)

  const dispatch = useDispatch()
  const globalOrders = useSelector(x => x.order.orders)
  const visits = useSelector(x => x.rv.visits)
  const { visitId } = qs.parse(props.location.search, {
    ignoreQueryPrefix: true
  })

  const actions = [
    {
      text: "Make Order",
      onClick: () => history.push("/make-order")
    }
  ]
  const columnDefs = [
    {
      headerName: "Item",
      field: "name",
      minWidth: 250,
      flex: 1
    },
    {
      headerName: "Quantity",
      field: "quantity",
      maxWidth: 120
    },
    {
      headerName: "State",
      field: "state",
      maxWidth: 110,
      cellRendererFramework: ({ data }) => {
        const currentState = data?.state

        const stateDict = {
          0: ["CREATED", "badge-light-info"],
          1: ["ACCEPTED", "badge-light-primary"],
          2: ["MAKING", "badge-secondary"],
          3: ["DONE", "badge-light-warning"],
          4: ["SERVED", "badge-light-success"],
          5: ["REJECTED", "badge-light-danger"],
          6: ["CANCELLED", "badge-light-danger"]
        }

        return (
          <div className={`badge badge-pill ${stateDict[currentState][1]}`}>
            {stateDict[currentState][0]}
          </div>
        )
      }
    },
    {
      headerName: "Order Time",
      field: "orderTime",
      maxWidth: 170,
      hide: user?.role === "CUSTOMER",
      cellRendererFramework: ({ data }) => {
        return (
          <div>
            {moment(data.orderTime).local().format("YYYY-MM-DD HH:mm:ss")}
          </div>
        )
      }
    },
    {
      headerName: "Note",
      field: "note",
      maxWidth: 400,
      hide: user?.role === "CUSTOMER",
      cellRendererFramework: ({ data }) => {
        return <div>{data?.note || "None"}</div>
      }
    },
    {
      headerName: "",
      field: "actions",
      maxWidth: 120,
      cellRendererFramework: ({ data }) => {
        return (
          <div className="actions cursor-pointer">
            {data?.processing === true ? (
              <Spinner color="primary" size="sm" />
            ) : (
              <React.Fragment>
                {data.state === 0 ? (
                  <Delete
                    className="mr-50"
                    size={15}
                    onClick={async () => {
                      await dispatch(
                        updateOrder({
                          id: data.id,
                          state: 6,
                          processing: true
                        })
                      )
                    }}
                  />
                ) : (
                  <div />
                )}
                {data.state === 6 ? (
                  <Eye
                    className="mr-50"
                    size={15}
                    onClick={async () => {
                      await dispatch(
                        updateOrder({
                          id: data.id,
                          state: 0,
                          processing: true
                        })
                      )
                    }}
                  />
                ) : (
                  <div />
                )}
              </React.Fragment>
            )}
          </div>
        )
      }
    }
  ]

  useEffect(() => {
    const filteredOrders = []
    Object.values(globalOrders).forEach(order => {
      if (order.visitId === currentVisit?.id) {
        filteredOrders.push(order)
      }
    })

    setOrdersOfCurrentVisit(filteredOrders)
  }, [globalOrders, currentVisit])

  useEffect(() => {
    if (visits[visitId]) {
      setCurrentVisit(visits[visitId])
    }
  }, [visits, visitId])

  useEffect(() => {
    if (!currentVisit?.storeId) return

    const initItemList = async () => {
      const itemList = (await itemService.getAllByStoreId(currentVisit.storeId))
        ?.data
      setStoreItems(itemList)
    }
    initItemList()
  }, [currentVisit])

  const toggle = tab => {
    if (active !== tab) setActive(tab)
  }

  return (
    <React.Fragment>
      <Nav tabs className="nav-justified">
        <NavItem>
          <NavLink
            className={classnames({
              active: active === "1"
            })}
            onClick={() => toggle("1")}
          >
            Menu
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: active === "2"
            })}
            onClick={() => toggle("2")}
          >
            Orders
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: active === "3"
            })}
            onClick={() => toggle("3")}
          >
            Payment
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={active}>
        <TabPane tabId="1">
          <MenuView storeItems={storeItems} visit={currentVisit} />
        </TabPane>
        <TabPane tabId="2">
          <FastTable
            rowData={ordersOfCurrentVisit}
            columnDefs={columnDefs}
            actions={actions}
          />
        </TabPane>
        <TabPane tabId="3">
          <Checkout
            visit={currentVisit}
            distinctOrders={ordersOfCurrentVisit}
            storeItems={storeItems}
          />
        </TabPane>
      </TabContent>
    </React.Fragment>
  )
}
export default OrderList
