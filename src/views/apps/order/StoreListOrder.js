import React, { useEffect, useState } from "react"
import FastTable from "@fasto/FastTable"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { updateOrder } from "../../../redux/actions/orderActions"
import { ZapOff, Eye, SkipForward } from "react-feather"
import { Nav, NavItem, NavLink, Spinner, TabContent, TabPane } from "reactstrap"
import classnames from "classnames"
import moment from "moment"

const StoreOrderList = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const [active, setActive] = useState("1")
  const [ordersGroupByItemAndState, setOrdersGroupByItemAndState] = useState([])
  const [distinctOrders, setDistinctOrders] = useState([])
  const globalOrderDict = useSelector(x => x.order.orders)
  const visits = useSelector(x => x.rv.visits)

  const actions = [
    {
      text: "Make Order",
      onClick: () => history.push("/make-order")
    }
  ]
  const ordersGroupByItemAndStateColumnDefs = [
    {
      headerName: "Item",
      field: "name",
      flex: 1,
      minWidth: 250
    },
    {
      headerName: "Quantity",
      field: "totalQuantities",
      maxWidth: 120
    },
    {
      headerName: "Total Orders",
      field: "totalOrders",
      maxWidth: 160
    },
    {
      headerName: "State",
      filter: true,
      field: "state",
      maxWidth: 170,
      cellRendererFramework: ({ data }) => {
        const currentState = data?.state

        const stateDict = {
          0: ["CREATED", "badge-light-info"],
          1: ["ACCEPTED", "badge-light-primary"],
          2: ["MAKING", "badge-secondary"],
          3: ["DONE", "badge-light-warning"],
          4: ["SERVED", "badge-light-success"],
          5: ["REJECTED", "badge-light-danger"],
          6: ["CANCELLED", "badge-light-danger"]
        }

        return (
          <div className={`badge badge-pill ${stateDict[currentState][1]}`}>
            {stateDict[currentState][0]}
          </div>
        )
      }
    }
  ]

  const distinctOrdersColumnDefs = [
    {
      headerName: "Item",
      field: "name",
      flex: 1,
      minWidth: 250
    },
    {
      headerName: "Visit Code",
      field: "visitCode",
      maxWidth: 150
    },
    {
      headerName: "Table",
      field: "table",
      maxWidth: 120
    },
    {
      headerName: "Quantity",
      field: "quantity",
      maxWidth: 120
    },
    {
      headerName: "State",
      filter: true,
      field: "state",
      maxWidth: 150,
      cellRendererFramework: ({ data }) => {
        const currentState = data?.state

        const stateDict = {
          0: ["CREATED", "badge-light-info"],
          1: ["ACCEPTED", "badge-light-primary"],
          2: ["MAKING", "badge-secondary"],
          3: ["DONE", "badge-light-warning"],
          4: ["SERVED", "badge-light-success"],
          5: ["REJECTED", "badge-light-danger"],
          6: ["CANCELLED", "badge-light-danger"]
        }

        return (
          <div className={`badge badge-pill ${stateDict[currentState][1]}`}>
            {stateDict[currentState][0]}
          </div>
        )
      }
    },
    {
      headerName: "Order Time",
      field: "orderTime",
      maxWidth: 170,
      cellRendererFramework: ({ data }) => {
        return (
          <div>
            {moment(data.orderTime).local().format("YYYY-MM-DD HH:mm:ss")}
          </div>
        )
      }
    },
    {
      headerName: "Note",
      field: "note",
      maxWidth: 250,
      cellRendererFramework: ({ data }) => {
        return <div>{data?.note || "None"}</div>
      }
    },
    {
      headerName: "",
      field: "actions",
      maxWidth: 170,
      cellRendererFramework: ({ data }) => {
        return (
          <div className="actions cursor-pointer">
            {data?.processing === true ? (
              <Spinner color="primary" size="sm" />
            ) : (
              <React.Fragment>
                <SkipForward
                  hidden={data.state >= 4}
                  className="mr-50"
                  size={15}
                  onClick={async () => {
                    if (data.state >= 4) return
                    await dispatch(
                      updateOrder({
                        id: data.id,
                        state: data.state + 1,
                        processing: true
                      })
                    )
                  }}
                />
                <ZapOff
                  hidden={data.state === 5}
                  className="mr-50"
                  size={15}
                  onClick={async () => {
                    await dispatch(
                      updateOrder({
                        id: data.id,
                        state: 5,
                        processing: true
                      })
                    )
                  }}
                />
                <Eye
                  hidden={[0, 1, 2, 3, 4, 6].includes(data.state)}
                  className="mr-50"
                  size={15}
                  onClick={async () => {
                    await dispatch(
                      updateOrder({
                        id: data.id,
                        state: 0,
                        processing: true
                      })
                    )
                  }}
                />
              </React.Fragment>
            )}
          </div>
        )
      }
    }
  ]

  useEffect(() => {
    // Set orders group by item
    const ordersGroupByItemDict = Object.values(globalOrderDict).reduce(
      (ordersGroupByItem, order) => {
        const key = `${order.itemId}&${order.state}`
        if (key in ordersGroupByItem) {
          ordersGroupByItem[key].totalOrders += 1
          ordersGroupByItem[key].totalQuantities += order.quantity
        } else {
          ordersGroupByItem[key] = {
            itemId: order.itemId,
            name: order.name,
            state: order.state,
            totalQuantities: order.quantity,
            totalOrders: 1
          }
        }

        return ordersGroupByItem
      },
      {}
    )
    setOrdersGroupByItemAndState(Object.values(ordersGroupByItemDict))

    // Set distinct orders
    setDistinctOrders(
      Object.values(globalOrderDict).map(order => {
        const visit = visits[order?.visitId]
        order.visitCode = visit?.code
        order.table = visit?.table

        return order
      })
    )
  }, [globalOrderDict, visits])

  const toggle = tab => {
    if (active !== tab) setActive(tab)
  }

  return (
    <React.Fragment>
      <Nav tabs className="nav-justified">
        <NavItem>
          <NavLink
            className={classnames({
              active: active === "1"
            })}
            onClick={() => toggle("1")}
          >
            Orders By Item
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({
              active: active === "2"
            })}
            onClick={() => toggle("2")}
          >
            Separate Orders
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={active}>
        <TabPane tabId="1">
          <FastTable
            rowData={ordersGroupByItemAndState}
            columnDefs={ordersGroupByItemAndStateColumnDefs}
            actions={actions}
          />
        </TabPane>
        <TabPane tabId="2">
          <FastTable
            rowData={distinctOrders}
            columnDefs={distinctOrdersColumnDefs}
            actions={actions}
          />
        </TabPane>
      </TabContent>
    </React.Fragment>
  )
}
export default StoreOrderList
