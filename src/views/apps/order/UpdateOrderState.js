import React from "react"
import FastForm from "../../../components/@fasto/FastForm"
import * as Yup from "yup"
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { updateOrder } from "../../../redux/actions/orderActions"

const schema = Yup.object().shape({})

const UpdateOrderState = props => {
  const dispatch = useDispatch()
  const history = useHistory()

  const { id } = props.match.params
  const orders = useSelector(x => x.order.orders)
  // const user = useSelector(x => x.auth)
  const order = orders[id]

  const definitions = [
    {
      headerName: "Id",
      field: "id",
      type: "text",
      disabled: true,
      value: id
    },
    {
      headerName: "State",
      field: "state",
      type: "select",
      value: order.state,
      selectOptions: [
        { value: 0, label: "Created" },
        { value: 1, label: "Accepted" },
        { value: 2, label: "Making" },
        { value: 3, label: "Done" },
        { value: 4, label: "Served" },
        { value: 5, label: "Rejected" },
        { value: 6, label: "Cancelled" }
      ]
    },
    {
      headerName: "Note",
      field: "note",
      type: "text"
    }
  ]
  const actions = [
    {
      text: "Update State",
      type: "submit"
    },
    {
      text: "Back",
      color: "warning",
      onClick: () => history.goBack()
    }
  ]

  const handleSubmit = async values => {
    await dispatch(
      updateOrder({
        ...values,
        processing: true
      })
    )
    history.goBack()
  }

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        validationSchema={schema}
        actions={actions}
      />
    </React.Fragment>
  )
}
export default UpdateOrderState
