import React, { useEffect } from "react"
import FastForm from "../../../components/@fasto/FastForm"
import * as Yup from "yup"
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import {
  checkInVisit,
  updateQrCodeResult
} from "../../../redux/actions/rvActions"

const schema = Yup.object().shape({
  peopleNumber: Yup.number().moreThan(0, "Must be greater than zero!")
})

const CheckInVisit = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const user = useSelector(x => x.auth)
  const qrCodeResult = useSelector(x => x.rv.qrCodeResult)

  const definitions = [
    {
      headerName: "Store",
      field: "storeName",
      value: qrCodeResult?.storeName,
      disabled: true
    },
    {
      headerName: "Table",
      field: "table",
      value: qrCodeResult?.table
    },
    {
      headerName: "Peoples",
      field: "peopleNumber",
      type: "number"
    }
  ]
  const actions = [
    {
      text: "Check in",
      type: "submit"
    },
    {
      text: "Scan again",
      color: "warning",
      onClick: () => {
        dispatch(updateQrCodeResult({}))
      }
    }
  ]

  const handleSubmit = async values => {
    let request
    if (qrCodeResult.reservationId) {
      request = { ...values, reservationId: qrCodeResult.reservationId }
    } else {
      request = {
        ...values,
        customerId: user.id,
        storeId: qrCodeResult.storeId
      }
    }

    await dispatch(
      checkInVisit(request)
    )
    history.push("/visits")
  }

  useEffect(() => {
    if (!qrCodeResult?.storeId) {
      history.push("/check-in-qr")
    }
  }, [qrCodeResult, history])

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        validationSchema={schema}
        actions={actions}
      />
    </React.Fragment>
  )
}
export default CheckInVisit
