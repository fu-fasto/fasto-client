import React, { useEffect } from "react"
import { useHistory } from "react-router-dom"
import QrReader from "react-qr-reader"
import { Row, Col, Modal, ModalHeader, ModalBody } from "reactstrap"
import { useDispatch, useSelector } from "react-redux"
import { updateQrCodeResult } from "../../../redux/actions/rvActions"

const CheckInVisitWithQrCode = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const qrCodeResult = useSelector(x => x.rv.qrCodeResult)

  const handleScan = result => {
    try {
      var qrCodeResult = JSON.parse(result)
      if (qrCodeResult?.storeId) {
        dispatch(updateQrCodeResult(qrCodeResult))
      }
    } finally {
    }
  }

  useEffect(() => {
    if (qrCodeResult?.storeId) {
      history.push("/check-in-visit")
    }
  }, [qrCodeResult, history])

  return (
    <React.Fragment>
      <Modal
        isOpen={true}
        toggle={() => {
          history.push("")
        }}
        scrollable={true}
        className="modal-dialog-centered flex"
      >
        <ModalHeader className="d-flex justify-content-center">
          Scan Here
        </ModalHeader>
        <ModalBody style={{ width: "300" }}>
          <Row>
            <Col>
              <QrReader
                delay={300}
                // showViewFinder={false}
                onError={() => {}}
                onScan={handleScan}
                style={{}}
              />
            </Col>
          </Row>
        </ModalBody>
      </Modal>
    </React.Fragment>
  )
}
export default CheckInVisitWithQrCode
