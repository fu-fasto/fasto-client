import React, { useState } from "react"
import { Row, Col, Modal, ModalHeader, ModalBody } from "reactstrap"
import { useSelector } from "react-redux"
import FastForm from "../../../components/@fasto/FastForm"
import rvService from "../../../services/rvService"

const GetCheckInQrCode = () => {
  const [isShowQrCode, setIsShowQrCode] = useState(false)
  const [qrResult, setQrResult] = useState({})
  const user = useSelector(x => x.auth)

  const definitions = [
    {
      headerName: "Store Id",
      field: "storeId",
      value: user?.storeId,
      disabled: true,
      hidden: true
    },
    {
      headerName: "Table",
      field: "table"
    }
  ]
  const actions = [
    {
      text: "Generate",
      type: "submit"
    }
  ]

  const handleSubmit = async values => {
    const result = await rvService.getCheckInQrCode(values)
    setQrResult(result)
    setIsShowQrCode(true)
  }

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        actions={actions}
      />
      <Modal
        isOpen={isShowQrCode}
        toggle={() => {
          setIsShowQrCode(false)
        }}
        scrollable={true}
        className="modal-dialog-centered flex"
      >
        <ModalHeader className="d-flex justify-content-center">
          QR Code
        </ModalHeader>
        <ModalBody>
          <Row>
            <Col className="d-flex justify-content-center">
              <img
                width="400"
                alt="Qr Code"
                src={`data:image/${qrResult?.type};base64, ${qrResult?.base64Code}`}
              />
            </Col>
          </Row>
        </ModalBody>
      </Modal>
    </React.Fragment>
  )
}
export default GetCheckInQrCode
