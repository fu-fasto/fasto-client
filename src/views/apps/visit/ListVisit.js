import React, { useEffect, useState } from "react"
import moment from "moment"
import FastTable from "@fasto/FastTable"
import { useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { Spinner } from "reactstrap"
import { Check, Coffee } from "react-feather"
import { useDispatch } from "react-redux"
import { checkOutVisit } from "@src/redux/actions/rvActions"

const ListVisit = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const visits = useSelector(x => x.rv.visits)
  const user = useSelector(x => x.auth)
  const [renderData, setRenderData] = useState([])

  const checkOut = visit => {
    dispatch(checkOutVisit(visit))
  }

  const actions = []
  const columnDefs = [
    {
      headerName: "Code",
      field: "code",
      width: 150
    },
    {
      headerName: user?.role === "CUSTOMER" ? "Store" : "Customer",
      field: user?.role === "CUSTOMER" ? "storeName" : "customerName",
      minWidth: 200,
      flex: 1
    },
    {
      headerName: "Table",
      field: "table",
      maxWidth: 100
    },
    {
      headerName: "Peoples",
      field: "peopleNumber",
      maxWidth: 110
    },
    {
      headerName: "Check-in Time",
      field: "checkInTime",
      maxWidth: 170,
      hide: user?.role === "CUSTOMER",
      cellRendererFramework: ({ data }) => {
        return (
          <div>
            {moment(data.checkInTime).local().format("YYYY-MM-DD HH:mm:ss")}
          </div>
        )
      }
    },
    {
      headerName: "Payment",
      field: "state",
      maxWidth: 120,
      cellRendererFramework: ({ data }) => {
        const currentState = data?.paymentTransactionState ?? false

        const stateDict = {
          true: ["PAID", "badge-light-success"],
          false: ["UNPAID", "badge-light-danger"],
        }

        return (
          <div className={`badge badge-pill ${stateDict[currentState][1]}`}>
            {stateDict[currentState][0]}
          </div>
        )
      }
    },
    {
      headerName: "Actions",
      field: "actions",
      maxWidth: 120,
      cellRendererFramework: ({ data }) => {
        return (
          <div className="actions align-items-center">
            {data?.processing === true ? (
              <Spinner color="primary" size="sm" />
            ) : user.role === "CUSTOMER" ? (
              <Coffee
                key="2"
                className="mr-50"
                size={15}
                onClick={() => history.push(`/orders?visitId=${data.id}`)}
              />
            ) : (
                <Check
                  key="1"
                  size={15}
                  onClick={() => checkOut({ ...data })}
                />
            )}
          </div>
        )
      }
    }
  ]

  if (user?.role.toUpperCase().includes("STORE")) {
    actions.push({
      text: "QR Code",
      onClick: () => history.push("/get-qr-code")
    })
  }

  useEffect(() => {
    setRenderData(Object.values(visits))
  }, [visits])

  return (
    <React.Fragment>
      <FastTable
        rowData={renderData}
        columnDefs={columnDefs}
        actions={actions}
      />
    </React.Fragment>
  )
}

export default ListVisit
