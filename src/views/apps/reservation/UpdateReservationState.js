import React from "react"
import FastForm from "../../../components/@fasto/FastForm"
import * as Yup from "yup"
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { updateReservation } from "../../../redux/actions/rvActions"

const schema = Yup.object().shape({})

const UpdateReservationState = props => {
  const dispatch = useDispatch()
  const history = useHistory()
  const { id } = props.match.params
  const reservations = useSelector(x => x.rv.reservations)
  const reservation = reservations[id]

  const definitions = [
    {
      headerName: "Id",
      field: "id",
      type: "text",
      disabled: true,
      value: id
    },
    {
      headerName: "State",
      field: "state",
      type: "select",
      value: reservation.state,
      selectOptions: [
        { value: 0, label: "Created" },
        { value: 1, label: "Accepted" },
        { value: 2, label: "Visited" },
        { value: 3, label: "Rejected" },
        { value: 4, label: "Cancelled" }
      ]
    },
    {
      headerName: "Note",
      field: "note",
      type: "text"
    }
  ]
  const actions = [
    {
      text: "Update State",
      type: "submit"
    },
    {
      text: "Back",
      color: "warning",
      onClick: () => history.goBack()
    }
  ]

  const handleSubmit = async values => {
    await dispatch(
      updateReservation({
        ...values,
        processing: true
      })
    )
    history.goBack()
  }

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        validationSchema={schema}
        actions={actions}
      />
    </React.Fragment>
  )
}
export default UpdateReservationState
