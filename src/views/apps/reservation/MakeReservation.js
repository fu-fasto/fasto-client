import React, { useEffect } from "react"
import FastForm from "../../../components/@fasto/FastForm"
import * as Yup from "yup"
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { makeReservation } from "../../../redux/actions/rvActions"

const schema = Yup.object().shape({
  peopleNumber: Yup.number().moreThan(0, "Must be greater than zero!"),
  expectedTime: Yup.date().required("Required")
})

const ReservationCreating = props => {
  const dispatch = useDispatch()
  const history = useHistory()
  const user = useSelector(x => x.auth)
  const { id: storeId } = props.match.params

  const definitions = [
    {
      headerName: "Peoples",
      field: "peopleNumber",
      type: "number",
      defaultValue: 1
    },
    {
      headerName: "Expected Time",
      field: "expectedTime",
      type: "datetime"
    },
    {
      headerName: "Note",
      field: "note",
      type: "text"
    }
  ]
  const actions = [
    {
      text: "Make reservation",
      type: "submit"
    },
    {
      text: "Back",
      color: "warning",
      onClick: () => history.push("/reservations")
    }
  ]

  useEffect(() => {
    if (user.role === "GUEST") {
      history.push(`/login?redirect=${props?.location?.pathname}`)
    }
  }, [user.role, history, props])

  const handleSubmit = async values => {
    await dispatch(
      makeReservation({
        ...values,
        storeId,
        customerId: user.id,
        processing: true
      })
    )
    history.push("/reservations")
  }

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        validationSchema={schema}
        actions={actions}
      />
    </React.Fragment>
  )
}
export default ReservationCreating
