import React, { useEffect, useState } from "react"
import moment from "moment"
import FastTable from "@fasto/FastTable"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { Spinner } from "reactstrap"
import {
  updateReservation,
  updateQrCodeResult
} from "../../../redux/actions/rvActions"
import * as Icon from "react-feather"

const ReservationList = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const reservations = useSelector(x => x.rv.reservations)
  const user = useSelector(x => x.auth)
  const [renderData, setRenderData] = useState([])

  const actions = []
  const columnDefs = [
    {
      headerName: "Code",
      field: "code",
      width: 150
    },
    {
      headerName: user.role === "CUSTOMER" ? "Store" : "Customer",
      field: user.role === "CUSTOMER" ? "storeName" : "customerName",
      flex: 1,
      minWidth: 200,
    },
    {
      headerName: "Peoples",
      field: "peopleNumber",
      maxWidth: 110
    },
    {
      headerName: "State",
      field: "state",
      maxWidth: 120,
      cellRendererFramework: ({ data }) => {
        const currentState = data?.state

        const stateDict = {
          0: ["CREATED", "badge-light-info"],
          1: ["ACCEPTED", "badge-light-primary"],
          2: ["VISITED", "badge-light-success"],
          3: ["REJECTED", "badge-light-warning"],
          4: ["CANCELLED", "badge-light-danger"]
        }

        return (
          <div className={`badge badge-pill ${stateDict[currentState][1]}`}>
            {stateDict[currentState][0]}
          </div>
        )
      }
    },
    {
      headerName: "Expected Time",
      field: "expectedTime",
      maxWidth: 170,
      hide: user?.role === "CUSTOMER",
      cellRendererFramework: ({ data }) => {
        return (
          <div>
            {moment(data.expectedTime).local().format("YYYY-MM-DD HH:mm:ss")}
          </div>
        )
      }
    },
    {
      headerName: "Note",
      field: "note",
      maxWidth: 500,
      hide: user?.role === "CUSTOMER",
      cellRendererFramework: ({ data }) => {
        const currentState = data?.state

        return <div>{currentState?.note || "None"}</div>
      }
    },
    {
      headerName: "Actions",
      field: "actions",
      cellRendererFramework: ({ data }) => {
        return (
          <div className="actions cursor-pointer">
            {data?.processing === true ? (
              <Spinner color="primary" size="sm" />
            ) : user.role === "CUSTOMER" ? (
              <React.Fragment>
                {data.state === 1 ? (
                  <Icon.Minimize
                    className="mr-50"
                    size={15}
                    onClick={() => {
                      dispatch(
                        updateQrCodeResult({
                          storeName: data.storeName,
                          storeId: data.storeId,
                          customerId: user.id,
                          reservationId: data.id
                        })
                      )
                      history.push("/check-in-visit")
                    }}
                  />
                ) : (
                  <div />
                )}
                {data.state === 0 ? (
                  <Icon.XSquare
                    hidden={data.state === 4}
                    className="mr-50"
                    size={15}
                    onClick={() => {
                      dispatch(
                        updateReservation({
                          id: data.id,
                          state: 4,
                          processing: true
                        })
                      )
                    }}
                  />
                ) : (
                  <div />
                )}
              </React.Fragment>
            ) : (
              <Icon.Edit
                className="mr-50"
                size={15}
                onClick={() =>
                  history.push(`/update-reservation-state/${data.id}`)
                }
              />
            )}
          </div>
        )
      }
    }
  ]

  useEffect(() => {
    setRenderData(Object.values(reservations))
  }, [reservations])

  return (
    <React.Fragment>
      <FastTable
        rowData={renderData}
        columnDefs={columnDefs}
        actions={actions}
      />
    </React.Fragment>
  )
}
export default ReservationList
