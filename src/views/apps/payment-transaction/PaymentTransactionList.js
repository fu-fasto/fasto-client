import React, { useEffect, useState } from "react"
import FastTable from "@fasto/FastTable"
import { useSelector } from "react-redux"
import moment from "moment"
import paymentService from "@src/services/paymentService"

function PaymentTransactionList() {
  const [renderData, setRenderData] = useState([])
  const user = useSelector(x => x.auth)

  const columnDefs = [
    {
      headerName: user.role === "CUSTOMER" ? "Store" : "Customer",
      field: user.role === "CUSTOMER" ? "storeName" : "customerName",
      flex: 1
    },
    {
      headerName: "Payment Method",
      field: "provider"
    },
    {
      headerName: "Price",
      field: "price",
      cellRendererFramework: ({ data }) => {
        return (
          <div>
            {data.currency === "USD" ? data.price.toFixed(2) : data.price}
          </div>
        )
      }
    },
    {
      headerName: "Currency",
      field: "currency"
    },
    {
      headerName: "Time",
      field: "createAt",
      cellRendererFramework: ({ data }) => {
        return (
          <div>
            {moment(data.createAt).local().format("DD-MM-YYYY HH:mm:ss")}
          </div>
        )
      }
    }
  ]

  useEffect(() => {
    async function getTransactions() {
      const findQuery = {
        state: 1
      }
      if (user.role.toUpperCase().includes("STORE")) {
        findQuery.storeId = user.storeId
      } else {
        findQuery.customerId = user.id
      }
      const transactions =
        (await paymentService.getPaymentTransactions(findQuery)) ?? []

      setRenderData(transactions)
    }

    getTransactions()
  }, [user])

  return (
    <React.Fragment>
      <FastTable rowData={renderData} columnDefs={columnDefs} />
    </React.Fragment>
  )
}

export default PaymentTransactionList
