import React, { useEffect, useState } from "react"
import FastTable from "@fasto/FastTable"
import { useHistory } from "react-router"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEdit } from "@fortawesome/free-solid-svg-icons"
import { useSelector } from "react-redux"
import paymentService from "@src/services/paymentService"

function PaymentAccountList() {
  const history = useHistory()
  const user = useSelector(x => x.auth)
  const [renderData, setRenderData] = useState([])

  const actions = [
    {
      text: "Create payment account",
      onClick: () => history.push("/create-payment-account")
    }
  ]

  const columnDefs = [
    {
      headerName: "Email",
      field: "accountData",
      filter: "agTextColumnFilter",
      flex: 1
    },
    {
      headerName: "Payment Provider",
      field: "provider",
      filter: "agTextColumnFilter",
      flex: 1
    },
    {
      headerName: "Is Activated",
      field: "isActivated",
      filter: "agTextColumnFilter",
      flex: 1
    },
    {
      headerName: "Actions",
      field: "actions",
      cellRendererFramework: ({ data }) => {
        return (
          <div className="actions cursor-pointer">
            <FontAwesomeIcon
              icon={faEdit}
              size="sm"
              onClick={() => history.push(`/update-payment-account/${data.id}`)}
            />
          </div>
        )
      }
    }
  ]

  useEffect(() => {
    async function loadData() {
      const findQuery = { storeId: user.storeId }
      const paymentAccounts =
        (await paymentService.getPaymentAccounts(findQuery)) ?? []

      setRenderData(paymentAccounts)
    }

    loadData()
  }, [user])

  return (
    <React.Fragment>
      <FastTable
        rowData={renderData}
        columnDefs={columnDefs}
        actions={actions}
      />
    </React.Fragment>
  )
}

export default PaymentAccountList
