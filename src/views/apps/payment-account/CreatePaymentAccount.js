import React from "react"
import FastForm from "../../../components/@fasto/FastForm"
import * as Yup from "yup"
import { useHistory } from "react-router-dom"
import {  useSelector } from "react-redux"
import paymentService from "@src/services/paymentService"

function CreatePaymentAccount() {
  const history = useHistory()
  const user = useSelector(x => x.auth)

  const schema = Yup.object().shape({
    accountData: Yup.string().required("Required"),
    provider: Yup.string().required("Required")
  })

  const definitions = [
    {
      headerName: "Payment provider",
      field: "provider",
      type: "select",
      filter: "agTextColumnFilter",
      selectOptions: [
        {
          value: "PayPal",
          label: "PayPal"
        },
        {
          value: "MoMo",
          label: "MoMo"
        }
      ]
    },
    {
      headerName: "Email",
      field: "accountData",
      filter: "agTextColumnFilter"
    }
  ]

  const actions = [
    {
      text: "Create",
      type: "submit"
    },
    {
      text: "Back",
      color: "warning",
      onClick: () => history.goBack()
    }
  ]

  const handleSubmit = async values => {
    values.storeId = user.storeId;
    await paymentService.createPaymentAccount(values);

    history.goBack()
  }

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        validationSchema={schema}
        actions={actions}
      />
    </React.Fragment>
  )
}

export default CreatePaymentAccount
