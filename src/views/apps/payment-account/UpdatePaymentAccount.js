import React, { useState, useEffect } from "react"
import { useHistory } from "react-router"
import FastForm from "../../../components/@fasto/FastForm"
import paymentService from "@src/services/paymentService"

function UpdatePaymentAccount(props) {
  const history = useHistory()
  const { id } = props.match.params
  const [definitions, setDefinitions] = useState([])

  const actions = [
    {
      text: "Update",
      type: "submit"
    },
    {
      text: "Back",
      color: "warning",
      onClick: () => history.goBack()
    }
  ]

  useEffect(() => {
    async function loadData() {
      const paymentAccount = await paymentService.getOnePaymentAccount(id)

      setDefinitions([
        {
          headerName: "Email",
          field: "accountData",
          filter: "agTextColumnFilter",
          value: paymentAccount.accountData
        },
        {
          headerName: "Payment Provider",
          field: "provider",
          value: paymentAccount.provider,
          disabled: true
        },
        {
          headerName: "Is Activated",
          field: "isActivated",
          type: "select",
          value: paymentAccount.isActivated,
          selectOptions: [
            { value: true, label: "Activate" },
            { value: false, label: "Disable" }
          ]
        }
      ])
    }

    loadData()
  }, [id])

  const handleSubmit = async values => {
    await paymentService.updatePaymentAccount(id, values)

    history.goBack()
  }

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        actions={actions}
      />
    </React.Fragment>
  )
}

export default UpdatePaymentAccount
