import React from "react"
import FastForm from "../../../components/@fasto/FastForm"
import * as Yup from "yup"
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { changePassword } from "../../../redux/actions/userActions"

const schema = Yup.object().shape({
  oldPwd: Yup.string().required("Required"),
  newPwd: Yup.string().required("Required"),
  retypeNewPwd: Yup.string().required("Required")
})

const ChangePwd = () => {
  const dispatch = useDispatch()
  const history = useHistory()

  const user = useSelector(x => x.auth)

  const definitions = [
    {
      headerName: "Old Password",
      field: "oldPwd",
      type: "password"
    },
    {
      headerName: "New Password",
      field: "newPwd",
      type: "password"
    },
    {
      headerName: "Enter new password again",
      field: "retypeNewPwd",
      type: "password"
    }
  ]
  const actions = [
    {
      text: "Change Password",
      type: "submit"
    },
    {
      text: "Back",
      color: "warning",
      onClick: () => history.goBack()
    }
  ]

  const handleSubmit = async values => {
    await dispatch(
      changePassword({
        ...values,
        id: user.id
      })
    )
    history.goBack()
  }

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        validationSchema={schema}
        actions={actions}
      />
    </React.Fragment>
  )
}
export default ChangePwd
