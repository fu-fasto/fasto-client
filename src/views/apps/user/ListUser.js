import React, { useEffect, useState } from "react"
import FastTable from "@fasto/FastTable"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEdit } from "@fortawesome/free-solid-svg-icons"
import { getUsers } from "../../../redux/actions/userActions"

function initUsers(dispatch, user) {
  dispatch(getUsers(user))
}

const UserList = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const currUser = useSelector(x => x.auth)
  const users = useSelector(x => x.user.users)
  const [renderData, setRenderData] = useState([])

  useEffect(() => {
    initUsers(dispatch, currUser)
  }, [dispatch, currUser])

  const actions = [
    {
      text: "Create User",
      onClick: () => history.push("/create-user")
    }
  ]
  const columnDefs = [
    {
      headerName: "Name",
      field: "name",
      filter: "agTextColumnFilter",
      flex: 1
    },
    {
      headerName: "Email",
      field: "email",
      filter: "agTextColumnFilter",
      flex: 1
    },
    {
      headerName: "Role",
      field: "role",
      filter: "agTextColumnFilter",
      cellRendererFramework: ({ data }) => {
        const currentState = data?.role.toUpperCase()

        const stateDict = {
          "CUSTOMER": ["CUSTOMER", "badge-light-info"],
          "STORE.ADMIN": ["STORE.ADMIN", "badge-light-primary"],
          "STORE.MANAGER": ["STORE.MANAGER", "badge-light-success"],
          "STORE.STAFF": ["STORE.STAFF", "badge-light-warning"],
          "FASTO.ADMIN": ["FASTO.ADMIN", "badge-light-danger"]
        }

        return (
          <div className={`badge badge-pill ${stateDict[currentState][1]}`}>
            {stateDict[currentState][0]}
          </div>
        )
      }
    },
    {
      headerName: "Actions",
      field: "actions",
      cellRendererFramework: ({ data }) => {
        return (
          <div className="actions cursor-pointer">
            <FontAwesomeIcon
              icon={faEdit}
              size="sm"
              onClick={() => history.push(`/update-user/${data.id}`)}
            />
          </div>
        )
      }
    }
  ]

  useEffect(() => {
    setRenderData(Object.values(users))
  }, [users])

  return (
    <React.Fragment>
      <FastTable
        rowData={renderData}
        columnDefs={columnDefs}
        actions={actions}
      />
    </React.Fragment>
  )
}
export default UserList
