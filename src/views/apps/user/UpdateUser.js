import React from "react"
import FastForm from "../../../components/@fasto/FastForm"
import * as Yup from "yup"
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { updateUser } from "../../../redux/actions/userActions"

const schema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Required"),
  name: Yup.string().required("Required")
})

const UpdateUser = props => {
  const dispatch = useDispatch()
  const history = useHistory()

  const { id } = props.match.params
  const users = useSelector(x => x.user.users)
  const user = users.filter(x => x.id === id)[0]

  const roles = useSelector(x => x.user.roles).map(role => ({
    value: role.id,
    label: role.roleName.toUpperCase()
  }))

  const definitions = [
    {
      headerName: "Id",
      field: "id",
      type: "text",
      disabled: true,
      value: id
    },
    {
      headerName: "Name",
      field: "name",
      value: user.name
    },
    {
      headerName: "Email",
      field: "email",
      value: user.email
    },
    {
      headerName: "Role",
      field: "roleId",
      type: "select",
      value: user.roleId,
      selectOptions: roles
    },
    {
      headerName: "StoreId",
      field: "storeId",
      value: user.storeId,
      hidden: user?.role?.includes("STORE")
    }
  ]
  const actions = [
    {
      text: "Update Information",
      type: "submit"
    },
    {
      text: "Back",
      color: "warning",
      onClick: () => history.goBack()
    }
  ]

  const handleSubmit = async values => {
    await dispatch(
      updateUser({
        ...values,
        role: roles
          .filter(x => x.value === values.roleId)[0]
          .label.toLowerCase()
      })
    )
    history.goBack()
  }

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        validationSchema={schema}
        actions={actions}
      />
    </React.Fragment>
  )
}
export default UpdateUser
