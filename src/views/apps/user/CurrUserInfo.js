import React from "react"
import FastForm from "../../../components/@fasto/FastForm"
import * as Yup from "yup"
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { updateUser } from "../../../redux/actions/userActions"

const schema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Required"),
  name: Yup.string().required("Required")
})

const CurrUserInfo = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const user = useSelector(x => x.auth)

  const definitions = [
    {
      headerName: "Name",
      field: "name",
      filter: "agTextColumnFilter",
      value: user.name
    },
    {
      headerName: "Email",
      field: "email",
      filter: "agTextColumnFilter",
      value: user.email
    }
  ]
  const actions = [
    {
      text: "Update Information",
      type: "submit"
    },
    {
      text: "Back",
      color: "warning",
      onClick: () => history.goBack()
    }
  ]

  const handleSubmit = async values => {
    //Need to have a different updateCurrUser//
    await dispatch(
      updateUser({
        ...values,
        id: user.id
      })
    )
    history.goBack()
  }

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        validationSchema={schema}
        actions={actions}
      />
    </React.Fragment>
  )
}
export default CurrUserInfo
