import React from "react"
import FastForm from "../../../components/@fasto/FastForm"
import * as Yup from "yup"
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { createUser } from "../../../redux/actions/userActions"

const schema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Required"),
  name: Yup.string().required("Required")
})

const UserCreating = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const roles = useSelector(x => x.user.roles).map(role => ({
    value: role.id,
    label: role.roleName.toUpperCase()
  }))

  const definitions = [
    {
      headerName: "Name",
      field: "name",
      filter: "agTextColumnFilter"
    },
    {
      headerName: "Email",
      field: "email",
      filter: "agTextColumnFilter"
    },
    {
      headerName: "Role",
      field: "roleId",
      type: "select",
      selectOptions: roles
    },
    {
      headerName: "StoreId",
      field: "storeId",
      filter: "agTextColumnFilter"
    }
  ]
  const actions = [
    {
      text: "Create user",
      type: "submit"
    },
    {
      text: "Back",
      color: "warning",
      onClick: () => history.goBack()
    }
  ]

  const handleSubmit = async values => {
    await dispatch(
      createUser({
        ...values,
        password: "123456"
      })
    )
    history.goBack()
  }

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        validationSchema={schema}
        actions={actions}
      />
    </React.Fragment>
  )
}
export default UserCreating
