import React, { useState } from 'react'
import {
    Card,
    CardImg,
    CardTitle,
    CardImgOverlay,
    Button
} from "reactstrap"
import "../../../assets/scss/pages/app-ecommerce-shop.scss"
import "swiper/css/swiper.css"
import img1 from "../../../assets/img/pages/food.jpg"
import { Link } from "react-router-dom"

const truncate = (str, n) => {
    return (str.length > n) ? str.substr(0, n - 1) + '...' : str;
}

const StoreCard = ({ stores }) => {
    const [idHover, setIdHover] = useState("");
    if (!stores) {
        return (<div></div>)
    }

    const mouseEnter = (id) => {
        setIdHover(id);
    }

    const mouseLeave = () => {
        setIdHover("");
    }

    return (
        <div className="ecommerce-application">
            <div className="grid-view wishlist-items">{
                stores.map((store) => {
                    return (
                        <Card className={`ecommerce-card`}
                            key={store.id}
                            onMouseEnter={() => mouseEnter(store.id)}
                            onMouseLeave={() => mouseLeave()}>
                            <CardImg top width="100%" src={ store?.imageUrl !== "" ? store?.imageUrl : img1} alt={store?.name + "image"} />
                            { 
                                store.id === idHover ?
                                    (<CardImgOverlay className="d-flex justify-content-center align-items-center">
                                        <Link to={`/app/store/detail/${store.id}`}>
                                            <Button color="warning">Reserve Now</Button>
                                        </Link>
                                    </CardImgOverlay>) :
                                    (<div></div>)
                            }
                            <CardTitle></CardTitle>
                            <div className="item-name">
                                <h5 className="my-1 text-bold-600">{truncate(store.name, 27)}</h5>
                                <h6>{truncate(store?.address + "- " + store?.district+ ", " +store?.city, 65)}</h6>
                            </div>
                        </Card>
                    )
                })}
            </div>
        </div>
    )
}

export default StoreCard
