import React, { useState, useEffect } from 'react'
import FastForm from "../../../components/@fasto/FastForm"
import * as Yup from "yup"
import { useHistory } from "react-router-dom"
import storeService from "../../../services/storeService"
import SweetAlert from 'react-bootstrap-sweetalert';
import "../../../assets/scss/plugins/extensions/sweet-alerts.scss"

const schema = Yup.object().shape({

})

// const convertOptions = (array, fieldName) => {
//   return array.map(item => ({
//     value: item.id,
//     label: item[fieldName]
//   }))
// }

const StoreForm = props => {
    const [alert, setAlert] = useState(false)

    useEffect(() => {

    }, []);

    const { store } = props.location.state;
    const { action } = props.location.state;

    const history = useHistory()

    const definitions = [
        {
            headerName: "Store name",
            field: "name",
            type: "text",
            value: store?.name
        },
        {
            headerName: "Phone number",
            field: "phoneNumber",
            type: "text",
            value: store?.phoneNumber
        },
        {
            headerName: "Customer Number",
            field: "customerNumber",
            type: "number",
            value: store?.customerNumber
        },
        {
            headerName: "Open Time",
            field: "openTime",
            type: "datetime",
            value: store?.openTime
        },
        {
            headerName: "Close Time",
            field: "closeTime",
            type: "datetime",
            value: store?.closeTime
        },
        {
            headerName: "Address",
            field: "address",
            type: "text",
            value: store?.address
        },
        {
            headerName: "Food region",
            field: "foodRegionId",
            type: "select",
            value: store?.foodRegionId,
            // selectOptions: foodRegions ? foodRegions : []
        },
        {
            headerName: "Store type",
            field: "storeTypeId",
            type: "select",
            value: store?.storeTypeId,
            // selectOptions: storeTypes ? storeTypes : []
        },
        {
            headerName: "Status",
            field: "statusId",
            type: "select",
            value: store?.statusId,
            // selectOptions: status ? status : []
        },
        {
            headerName: "City",
            field: "city",
            type: "text",
            value: store?.location?.city
        },
        {
            headerName: "District",
            field: "district",
            type: "text",
            value: store?.location?.district
        },
        {
            headerName: "Ward",
            field: "ward",
            type: "text",
            value: store?.location?.ward
        },
        {
            headerName: "Description",
            field: "description",
            type: "text",
            value: store?.description
        },
        {
            headerName: "Star",
            field: "star",
            type: "number",
            value: store?.star
        }
    ]

    if (action === "Edit") {
        definitions.unshift({
            headerName: "Store Id",
            field: "id",
            type: "text",
            value: store?.id,
            disabled: true
        },
            {
                headerName: "Location Id",
                field: "locationId",
                type: "text",
                value: store?.locationId,
                disabled: true
            })
    }

    const handleAlert = () => {
        setAlert(!alert)
    }

    const actions = [
        {
            text: action + " store",
            type: "submit"
        },
        {
            text: "Back",
            color: "warning",
            onClick: () => history.goBack()
        }
    ]
    const handleSubmit = async values => {
        if (action === "Edit") {
            await storeService.editStore(values)
            handleAlert()

        } else if (action === "Create") {
            await storeService.addStore(values)
            handleAlert()
        }


    }
    return (
        <React.Fragment>
            <FastForm
                definitions={definitions}
                onSubmit={handleSubmit}
                validationSchema={schema}
                actions={actions}
            />
            <SweetAlert title="Success"
                success
                show={alert}
                reverseButtons
                confirmBtnText="OK"
                onConfirm={() => {
                    history.goBack()
                    handleAlert()
                }}
                onCancel={() => {
                    handleAlert()
                }}
            >
                {action} success!
            </SweetAlert>

        </React.Fragment>
    )
}

export default StoreForm
