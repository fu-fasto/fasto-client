/* eslint-disable no-unused-vars */
import React, { useState } from "react"
import { Card, CardHeader, CardBody } from "reactstrap"
import { Map, TileLayer, Marker } from "react-leaflet"
import "../../../assets/scss/plugins/extensions/maps.scss"

const StoreMap = ({ props }) => {
  const [coordinates, setCoordinates] = useState({
    center: [16.044247, 108.215859],
    zoom: 12
  })

  if (!props) {
    return <div></div>
  }

  const district = props.district
  const city = props.city
  const address = props.address

  return (
    <Card className="overflow-hidden">
      <CardHeader>
        <h4>
          {address}, {district}, {city}
        </h4>
      </CardHeader>
      <CardBody>
        <Map center={coordinates.center} zoom={coordinates.zoom}>
          <TileLayer
            attribution='&ampcopy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.osm.org/{z}/{x}/{y}.png"
          />
          <Marker position={coordinates.center} />
        </Map>
      </CardBody>
    </Card>
  )
}

export default StoreMap
