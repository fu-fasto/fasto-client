import React from 'react'
import {
    Card,
    CardImg,
    CardTitle,
    Spinner
} from "reactstrap"
import "../../../assets/scss/pages/app-ecommerce-shop.scss"
import "swiper/css/swiper.css"

const formatCurrency = (money) =>
    String(money).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');

const MenuList = ({ items }) => {
    if (!items) {
        return (<div className="text-center">
            <Spinner className="mr-50" color="primary" />
        </div>)
    }
    return (
        <div className="ecommerce-application">
            <div className="grid-view wishlist-items">{
                items.map((item) => {
                    return (
                        <Card
                            key={item.id}>
                            <CardImg top width="100" height="200" src={item.imageUrl} alt={item.name + "image"} />
                            <CardTitle></CardTitle>
                            <div className="item-name">
                                <h6 className="my-1 text-bold-600">{item.name}</h6>
                                <span className="text-danger text-bold-600">{formatCurrency(item.price)} VND</span>
                            </div>
                        </Card>
                    )
                })}
            </div>
        </div>
    )
}

export default MenuList
