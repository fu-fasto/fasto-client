import React, { useState, useEffect } from "react"
import { Link } from "react-router-dom"
import FastTable from "../../../components/@fasto/FastTable"
import { useHistory } from "react-router-dom"
import itemService from "../../../services/itemService"
import {
  Input,
  Button,
  Card,
  CardBody,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Spinner
} from "reactstrap"
import { Edit, Delete } from "react-feather"

const formatCurrency = money =>
  String(money).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")

const MenuItemList = props => {
  const history = useHistory()
  const [mod, setMod] = useState(false)
  const [modalData, setModalData] = useState()
  const [items, setItems] = useState([])
  const { id } = props.match.params

  useEffect(() => {
    itemService.getAllByStoreId(id).then(response => {
      setItems(response.data)
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (!items) {
    return (
      <div className="text-center">
        <Spinner className="mr-50" color="primary" />
      </div>
    )
  }

  const toggleModal = () => {
    setMod(!mod)
  }

  let del = ""
  const handleInputChange = event => {
    del = event.target.value
  }

  const confirmDelete = () => {
    if (del === modalData.name) {
      //request delete
      toggleModal()
      setModalData({})
    } else {
      //khong xoa
    }
    del = ""
  }

  const columnStyle = [
    {
      headerName: "Item name",
      field: "name",
      flex: 1
    },
    {
      headerName: "Price",
      field: "price",
      cellRendererFramework: ({ data }) => {
        return <div>{formatCurrency(data.price)} VND</div>
      }
    },

    {
      headerName: "Description",
      field: "description"
    },
    {
      header: "Action",
      field: "action",
      cellRendererFramework: ({ data }) => {
        return (
          <div>
            <Link
              to={{
                pathname: `/app/store/addUpdateItemForm`,
                state: {
                  item: data,
                  action: "Edit",
                  storeId: data.storeId
                }
              }}
            >
              <Button className="mr-1 mb-1" size="sm" color="primary">
                <Edit size={15} />
              </Button>
            </Link>
            <Button
              className="mr-1 mb-1"
              size="sm"
              color="danger"
              onClick={() => {
                setModalData(data)
                toggleModal()
              }}
            >
              <Delete size={15} />
            </Button>
          </div>
        )
      }
    }
  ]
  const actions = [
    {
      text: "Create item",
      onClick: () =>
        history.push({
          pathname: "/app/store/createItem",
          state: {
            item: null,
            action: "Create",
            storeId: items[0].storeId
          }
        })
    }
  ]

  return (
    <div>
      <Card>
        <CardBody>
          <FastTable
            rowData={items}
            columnDefs={columnStyle}
            actions={actions}
          />
        </CardBody>
        <Modal
          isOpen={mod}
          toggle={toggleModal}
          className="modal-dialog-centered"
        >
          <ModalHeader toggle={toggleModal}>Do you sure to delete?</ModalHeader>
          <ModalBody>
            <h3>Enter "{modalData ? modalData.name : ""}" to delete</h3>
            <Input
              type="text"
              id="confirmDelete"
              name="confirmDelete"
              placeholder={modalData ? modalData.name : ""}
              onChange={handleInputChange}
            />
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={confirmDelete}>
              Confirm delete
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </Card>
    </div>
  )
}

export default MenuItemList
