import React from 'react'
import { Card, CardBody, CardHeader, CardTitle } from "reactstrap"

const StoreDescription = ({ description }) => {
    return (
        <Card>
            <CardHeader>
                <CardTitle>Description</CardTitle>
            </CardHeader>
            <CardBody>
                <p className="text-justify pt-2 mb-0">
                    {description}
                </p>
            </CardBody>
        </Card>
    )
}

export default StoreDescription
