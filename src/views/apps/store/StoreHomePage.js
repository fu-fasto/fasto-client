import React, { useEffect, useState } from "react"
import { Spinner } from "reactstrap"
import "../../../assets/scss/pages/app-ecommerce-shop.scss"
import "swiper/css/swiper.css"
import "../../../assets/scss/pages/knowledge-base.scss"
import storeService from "../../../services/storeService"
import StoreSlide from "./StoreSlides"
import StoreList from "./ListStore"
// import PromotionList from "./PromotionList"
import CollectionCard from "./StoreCollection"
import StoreListTab from "./StoreListTab"

const StoreHomePage = () => {
  const [stores, setStores] = useState([])

  useEffect(() => {
    storeService.getAllStore().then(res => {
      setStores(res.data.items)
    })
  }, [])

  if (!stores) {
    return (
      <div className="text-center">
        <Spinner className="mr-50" color="primary" />
      </div>
    )
  }

  return (
    <React.Fragment>
      <StoreSlide stores={null} /> <hr />
      <CollectionCard /> <hr />
      {/* <PromotionList stores={stores} /> <hr /> */}
      <StoreList stores={stores} /> <hr />
      <StoreListTab stores={stores} /> <hr />
    </React.Fragment>
  )
}
export default StoreHomePage
