import React, { useState, useEffect } from 'react'
import { EditorState, ContentState, convertToRaw} from "draft-js"
import { Editor } from "react-draft-wysiwyg"

const EditorContent = ({ description }) => {

    const [editorState, setEditorState] = useState( EditorState.createEmpty())

    useEffect(() => {
        setEditorState(EditorState.createWithContent(ContentState.createFromText(description)))
    },[description]);

    const blocks = convertToRaw(editorState.getCurrentContent()).blocks;
    const values = blocks.map(block => (!block.text.trim() && '\n') || block.text).join('\n');
    const onEditorStateChange = (value) => {
        setEditorState(value);
        console.log("editor state",values)
        console.log("block", blocks)
    }
    
    return (
        <Editor
            wrapperClassName="demo-wrapper"
            editorClassName="demo-editor"
            editorState={editorState}
            onEditorStateChange={onEditorStateChange}
        />
    )
}

export default EditorContent
