import React from "react"
import { Card, CardBody } from "reactstrap"
import Swiper from "react-id-swiper"

const params = {
  spaceBetween: 30,
  centeredSlides: true,
  autoplay: {
    delay: 2500,
    disableOnInteraction: false
  },
  pagination: {
    el: ".swiper-pagination",
    clickable: true
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev"
  }
}

const imageList = [
  "https://pasgo.vn/Upload/anh-slide-show/akataiyo-mat-troi-do-15-nam-kinh-nghiem-and-250-mon-an-truyen-thong-nhat-ban-123901791403.jpg",
  "https://pasgo.vn/Upload/anh-slide-show/com-nieu-3-ca-bong-com-nieu-da-nang-30-nam-tuoi-124086661404.jpg",
  "https://pasgo.vn/Upload/anh-slide-show/happy-womens-day-danh-yeu-thuong-cho-mot-nua-the-gioi-175845301552.jpg",
  "https://pasgo.vn/Upload/anh-slide-show/thai-market-huong-vi-am-thuc-thai-bung-no-day-say-me-98688781246.jpg",
  "https://pasgo.vn/Upload/anh-slide-show/hai-san-be-anh-1-thuong-hieu-hai-san-gan-20-nam-122372461461.jpg"
]

const StoreSlides = ({ store, currentUser }) => {
  return (
    <Card>
      <CardBody style={{ backgroundColor: "black" }}>
        <Swiper {...params}>
          {imageList.map((item, i) => {
            return (
              <div
                key={i}
                className="d-flex justify-content-center align-items-center"
              >
                <img
                  style={{ objectFit: "cover" }}
                  src={item}
                  alt={"swiper " + (i + 1)}
                />
              </div>
            )
          })}
        </Swiper>
      </CardBody>
      <hr style={{ marginTop: 0, marginBottom: 0 }} />
    </Card>
  )
}

export default StoreSlides
