import React from 'react'
import {
    Card,
    CardBody,
    Spinner,
    Row,
    Col
} from "reactstrap"
import Swiper from "react-id-swiper"
import "swiper/css/swiper.css"
import "../../../assets/scss/pages/app-ecommerce-shop.scss"
import { Link } from "react-router-dom"

const swiperParams = {
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
    },
    breakpoints: {
        1600: {
            slidesPerView: 5,
            spaceBetween: 55
        },
        1300: {
            slidesPerView: 4,
            spaceBetween: 55
        },
        1260: {
            slidesPerView: 3,
            spaceBetween: 55
        },
        900: {
            slidesPerView: 3,
            spaceBetween: 55
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 55
        },
        375: {
            slidesPerView: 1,
            spaceBetween: 55
        }
    }
}

const PromotionList = ({ stores }) => {

    if (!stores) {
        return (
            <div className="text-center">
                <Spinner className="mr-50" color="primary" />
            </div>
        )
    }

    return (
        <Row>
            <Col className="details-page-swiper mt-10" sm="12">
                <Swiper {...swiperParams}>
                    {stores.map((store) => {
                        return (
                            <Card key={store.id} style={{ width: '30rem', marginRight: 10, marginLeft: 10, padding: 0 }}>
                                <Link to={`/app/store/detail/${store.id}`}>
                                    <img
                                        src={store.imageUrl}
                                        alt="abc"
                                        className="mx-auto mb-2"
                                        width="100%"
                                    />

                                    <CardBody style={{ paddingTop: 0 }}>
                                        <h5 className="text-bold-600 warning"> Hot deal 50% </h5>
                                        <hr />
                                        <h4 className="text-bold-600">{store.name}</h4>
                                        <h5>{store.address}, {store.district}, {store.city}</h5>
                                    </CardBody>

                                </Link>
                            </Card>
                        )
                    })}
                </Swiper>
            </Col>
        </Row>
    )
}

export default PromotionList
