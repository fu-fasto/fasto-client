import React from 'react'
import {
    Row,
    Col,
    Card,
    CardBody,
} from "reactstrap"
import "swiper/css/swiper.css"
import { PlusSquare, Edit, Delete } from "react-feather"
import Swiper from "react-id-swiper"
import "../../../assets/scss/pages/app-ecommerce-shop.scss"


const formatCurrency = (money) =>
    String(money).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');

const swiperParams = {
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
    },
    breakpoints: {
        1600: {
            slidesPerView: 5,
            spaceBetween: 55
        },
        1300: {
            slidesPerView: 4,
            spaceBetween: 55
        },
        1260: {
            slidesPerView: 3,
            spaceBetween: 55
        },
        900: {
            slidesPerView: 3,
            spaceBetween: 55
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 55
        },
        375: {
            slidesPerView: 1,
            spaceBetween: 55
        }
    }
}
const ScrollList = ({ props }) => {
    if (!props) {
        return (<div></div>)
    }
    const listItem = props.listItem;
    const action = props.action;
    if (!listItem) {
        return (<div></div>)
    }
    return (
        <div>
            <Col sm="12">
                <Card className="overflow-hidden app-ecommerce-details">
                    <CardBody>
                        <Row>
                            <Col className="details-page-swiper text-center mt-5" sm="12">
                                <Swiper {...swiperParams}>
                                    {action === "edit" ?
                                        (<div>
                                            <div className="title mb-1">
                                            </div>
                                            <div className="img-container">
                                                <PlusSquare size={100} />
                                            </div>
                                            <p className="text-bold-500 font-medium-2 text-primary mt-50">
                                                Thêm món ăn
                                                    </p>
                                        </div>) : (<div></div>)}

                                    {listItem ? (listItem.map((item) => (
                                        <div key={item.id}>
                                            <div className="title mb-1">
                                                <p className="font-medium-1 text-bold-600 truncate mb-0">
                                                    {item.name}
                                                </p>
                                            </div>
                                            <div className="img-container">
                                                <img src={item.image} alt="watch" height="112" weight="112" />
                                            </div>
                                            <p className="text-bold-500 font-medium-2 text-primary mt-50">
                                                {formatCurrency(item.price)} VNĐ
                                                    </p>
                                            {action === "edit" ? (<div className="item-options text-center">
                                                <Row>
                                                    <Col md="6" sm="12" >
                                                        <div className="item-options text-center" color="primary">
                                                            <div className="cart">
                                                                <Edit size={15} />
                                                                <span className="align-middle ml-50">Sửa</span>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                    <Col md="6" sm="12" >
                                                        <div className="item-options text-center" color="danger">
                                                            <div className="cart">
                                                                <Delete size={15} />
                                                                <span className="align-middle ml-50">Xoá</span>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                </Row>

                                            </div>) : (<div></div>)}
                                        </div>
                                    ))) : (<div>no data</div>)}
                                </Swiper>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </Col>

        </div>
    )
}

export default ScrollList
