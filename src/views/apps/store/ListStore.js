import React from 'react'
import img1 from "../../../assets/img/pages/food.jpg"
import {
    Card,
    CardBody,
    CardImg,
    Spinner
} from "reactstrap"
import {
    ShoppingCart
} from "react-feather"
import { Link } from "react-router-dom"

const truncate = (str, n) => {
    return (str.length > n) ? str.substr(0, n - 1) + '...' : str;
}

const ListStore = ({ stores }) => {

    if (!stores) {
        return (<div className="text-center">
            <Spinner className="mr-50" color="primary" />
        </div>)
    }

    return (
        <div>
            <div className="ecommerce-application">
                <div className="grid-view wishlist-items">{
                    stores.map((store) => {
                        return (
                            <Card
                                className={`ecommerce-card`}
                                key={store.id}>
                                <div className="card-content text-center">
                                    <CardBody className="text-center">
                                        <Link to={`/app/store/detail/${store.id}`}>
                                            <CardImg
                                                className="img-fluid mb-2"
                                                src={ store?.imageUrl !== "" ? store?.imageUrl : img1}
                                                alt="card image cap"
                                                width="300rem"
                                                height="200rem"
                                            />

                                            <h4 className="text-bold-600">{truncate(store?.name, 23)}</h4>
                                            <h5>{truncate(store?.address, 27)}</h5>
                                            <h5>{store?.district}, {store?.city}</h5>
                                        </Link>
                                    </CardBody>
                                    <Link to={`/app/store/detail/${store.id}`}>
                                        <div className="item-options text-center">
                                            <div className="cart">
                                                <ShoppingCart size={20} />
                                                <span className="align-middle ml-50">Reserve Now</span>
                                            </div>
                                        </div>
                                    </Link>
                                </div>
                            </Card>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}

export default ListStore
