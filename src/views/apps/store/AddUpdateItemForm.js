import React, { useState } from "react"
import FastForm from "../../../components/@fasto/FastForm"
import { useHistory } from "react-router-dom"
import * as Yup from "yup"
import itemService from "../../../services/itemService"
import "../../../assets/scss/plugins/extensions/sweet-alerts.scss"
import SweetAlert from "react-bootstrap-sweetalert"

const schema = Yup.object().shape({})

const AddUpdateItemForm = props => {
  const history = useHistory()
  const [alert, setAlert] = useState(false)
  if (!props) {
    return <div></div>
  }

  const handleAlert = () => {
    setAlert(!alert)
  }

  const { item } = props.location.state
  const { action } = props.location.state
  const { storeId } = props.location.state

  const definitions = [
    {
      headerName: "Store Id",
      field: "storeId",
      type: "text",
      value: storeId,
      disabled: true,
      hidden: true
    },
    {
      headerName: "Item name",
      field: "name",
      type: "text",
      value: item?.name
    },
    {
      headerName: "Price",
      field: "price",
      type: "number",
      value: item?.price
    },
    {
      headerName: "Description",
      field: "description",
      type: "text",
      value: item?.description
    },
    {
      headerName: "Image",
      field: "imagesUrl",
      type: "text",
      value: item?.imagesUrl
    }
  ]

  if (action === "Edit") {
    definitions.unshift({
      headerName: "Item Id",
      field: "id",
      type: "text",
      value: item?.id,
      disabled: true,
      hidden: true
    })
  }

  const actions = [
    {
      text: action + " item",
      type: "submit"
    },
    {
      text: "Back",
      color: "warning",
      onClick: () => history.goBack()
    }
  ]

  const handleSubmit = async values => {
    if (action === "Edit") {
      await itemService.editItem(values)
      handleAlert()
    } else if (action === "Create") {
      await itemService.createOneItem(values)
      handleAlert()
    }
  }

  return (
    <React.Fragment>
      <FastForm
        definitions={definitions}
        onSubmit={handleSubmit}
        validationSchema={schema}
        actions={actions}
      />
      <SweetAlert
        title="Success"
        success
        show={alert}
        reverseButtons
        confirmBtnText="OK"
        onConfirm={() => {
          history.goBack()
          handleAlert()
        }}
        onCancel={() => {
          handleAlert()
        }}
      >
        {action} success!
      </SweetAlert>
    </React.Fragment>
  )
}

export default AddUpdateItemForm
