import React, { useState, useEffect } from "react"
import StoreList from "./ListStore"
import "../../../assets/scss/pages/app-ecommerce-shop.scss"
import qs from "qs"
import ReactPaginate from "react-paginate"
import { useHistory } from "react-router-dom"
import storeService from "../../../services/storeService"
import "../../../assets/scss/plugins/extensions/react-paginate.scss"
import { Row, Col } from "reactstrap"
import { ChevronLeft, ChevronRight } from "react-feather"
import FilterCard from "./FilterCard"

const SearchResultPage = props => {
  const history = useHistory()
  const [searchResult, setSearchResult] = useState({
    currentPage: 0,
    items: [],
    totalItems: 0,
    totalPages: 0
  })
  const [findQuery, setFindQuery] = useState(
    qs.parse(props.location.search, {
      ignoreQueryPrefix: true
    })
  )

  useEffect(() => {
    storeService.searchStore(findQuery).then(res => {
      setSearchResult(res.data)
    })
  }, [findQuery])

  useEffect(() => {
    history.push({
      pathname: `/app/store/search`,
      search: new URLSearchParams(findQuery).toString()
    })
  }, [findQuery, history])

  const changePage = ({ selected }) => {
    setFindQuery({
      ...findQuery,
      page: selected
    })

    window.scrollTo(0, 0)
  }

  const filterStores = (selectionType, selectValue) => {
    if (!selectValue) {
      delete findQuery[selectionType]
      setFindQuery({
        ...findQuery
      })
      return
    }

    if (selectionType === "cityId" || selectionType === "districtId") {
      setFindQuery({
        ...findQuery,
        [selectionType]: selectValue.value
      })
    } else {
      setFindQuery({
        ...findQuery,
        [selectionType]: [selectValue.value]
      })
    }
  }

  const applyFilter = () => {}

  const resetFilter = () => {
    setFindQuery({})
  }

  if (!props) {
    return <div></div>
  }

  return (
    <div>
      <Row>
        <Col>
          <h4 className="secondary">
            {searchResult
              ? `${searchResult?.items?.length} over total ${searchResult?.totalItems} results`
              : ""}
          </h4>
        </Col>
      </Row>
      <Row>
        <Col></Col>
      </Row>
      <hr />
      <FilterCard
        findQuery={findQuery}
        onFilter={filterStores}
        applyFilter={applyFilter}
        onReset={resetFilter}
      />
      <hr />
      <StoreList stores={searchResult?.items} />
      <Col sm="12">
        <div className="ecommerce-pagination">
          <ReactPaginate
            previousLabel={<ChevronLeft size="15" />}
            nextLabel={<ChevronRight size="15" />}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={searchResult?.totalPages}
            onPageChange={selectedItem => changePage(selectedItem)}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            containerClassName={
              "vx-pagination separated-pagination pagination-center mt-3"
            }
            activeClassName={"active"}
          />
        </div>
      </Col>
    </div>
  )
}

export default SearchResultPage
