import React, { useState } from "react"
import {
  Card,
  Row,
  Col,
  CardBody,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Spinner,
  Button
} from "reactstrap"
import { Link } from "react-router-dom"
import classnames from "classnames"
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css"
import "../../../assets/scss/plugins/extensions/editor.scss"
import EditForm from "./EditorContent"
import { Eye } from "react-feather"
import MenuList from "./MenuList"

const StoreTabContent = ({ store, currentUser, menu }) => {
  const [tab, setTab] = useState({
    active: "1"
  })
  const [editorStatus, setEditorStatus] = useState(false)

  if (!store) {
    return (
      <div className="text-center">
        <Spinner className="mr-50" color="primary" />
      </div>
    )
  }

  const toggle = tab => {
    if (tab.active !== tab) {
      setTab({ active: tab })
    }
  }

  const changeEditorStatus = () => {
    setEditorStatus(!editorStatus)
  }

  return (
    <Card>
      <CardBody>
        <TabContent activeTab={tab.activeTab}>
          <Nav tabs>
            <NavItem>
              <NavLink
                className={classnames({
                  active: tab.active === "1"
                })}
                onClick={() => {
                  toggle("1")
                }}
              >
                Menu
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({
                  active: tab.active === "2"
                })}
                onClick={() => {
                  toggle("2")
                }}
              >
                Description
              </NavLink>
            </NavItem>
            {/* <NavItem>
              <NavLink
                className={classnames({
                  active: tab.active === "3"
                })}
                onClick={() => {
                  toggle("3")
                }}
              >
                Promotion
              </NavLink>
            </NavItem> */}
          </Nav>
          <TabContent className="py-50" activeTab={tab.active}>
            <TabPane tabId="1">
              <MenuList items={menu} />
              {menu.length === 0 ? (
                <div className="text-center">Menu is empty</div>
              ) : (
                <div></div>
              )}
              {currentUser.role === "FASTO.ADMIN" ||
              currentUser.role === "STORE.ADMIN" ? (
                menu.length > 0 ? (
                  <div className="justify-content-between">
                    <Link to={`/app/store/listItemTable/${store.id}`}>
                      <Button className="mr-1 mb-1" color="primary">
                        <Eye size={15} />
                        <span className="align-middle ml-50">
                          List detail item
                        </span>
                      </Button>
                    </Link>
                  </div>
                ) : (
                  <div className="justify-content-between">
                    <Link
                      to={{
                        pathname: `/app/store/createItem`,
                        state: {
                          item: null,
                          action: "Create",
                          storeId: store.id
                        }
                      }}
                    >
                      <Button className="mr-1 mb-1" color="danger">
                        <Eye size={15} />
                        <span className="align-middle ml-50">Create item</span>
                      </Button>
                    </Link>
                  </div>
                )
              ) : (
                <div></div>
              )}
            </TabPane>
            <TabPane tabId="2">
              {editorStatus ? (
                <Row>
                  <Col sm="12">
                    <EditForm description={store.description} />
                  </Col>
                  <NavLink
                    onClick={() => {
                      changeEditorStatus()
                    }}
                  >
                    Update description
                  </NavLink>
                </Row>
              ) : (
                <div>
                  {store.description}
                  <NavLink
                    onClick={() => {
                      changeEditorStatus()
                    }}
                  >
                    Edit description
                  </NavLink>
                </div>
              )}
            </TabPane>
            <TabPane tabId="3">
              Biscuit icing jelly halvah apple pie croissant cookie. Toffee
              chupa chups brownie dessert biscuit wafer lemon drops. Bear claw
              jujubes I love I love. Marshmallow apple pie sugar plum chocolate
              cake.Dragée cake muffin. I love I love apple pie biscuit carrot
              cake croissant macaroon candy. Cheesecake I love cupcake I love
              candy canes I love. Cupcake macaroon bonbon biscuit jelly sesame
              snaps tart I love gingerbread.Cupcake cookie bear claw pie cotton
              candy gummies.
            </TabPane>
          </TabContent>
        </TabContent>
      </CardBody>
    </Card>
  )
}

export default StoreTabContent
