import React, { useState } from 'react'

import classnames from "classnames"
import {
    Card,
    CardBody,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Spinner
} from "reactstrap"
import StoreCard from "./StoreCard"

const StoreListTab = ({ stores }) => {
    const [tab, setTab] = useState({
        active: "1"
    });
    if (!stores) {
        return (
            <div className="text-center">
                <Spinner className="mr-50" color="primary" />
            </div>
        )
    }

    const toggle = tab => {
        if (tab.active !== tab) {
            setTab({ active: tab })
        }
    }
    return (
        <div>
            <Card>
                <CardBody>
                    <TabContent activeTab={tab.activeTab}>
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    className={classnames({
                                        active: tab.active === "1"
                                    })}
                                    onClick={() => {
                                        toggle("1")
                                    }}
                                >
                                    FEATURED
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({
                                        active: tab.active === "2"
                                    })}
                                    onClick={() => {
                                        toggle("2")
                                    }}
                                >
                                    TOP
                            </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({
                                        active: tab.active === "3"
                                    })}
                                    onClick={() => {
                                        toggle("3")
                                    }}
                                >
                                    NEAR BY
                            </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent className="py-50" activeTab={tab.active}>
                            <TabPane tabId="1">
                                <StoreCard stores={stores} />
                            </TabPane>
                            <TabPane tabId="2">
                                <StoreCard stores={stores} />
                            </TabPane>
                            <TabPane tabId="3">
                                <StoreCard stores={stores} />
                            </TabPane>
                        </TabContent>
                    </TabContent>
                </CardBody>
            </Card>
        </div>

    )
}

export default StoreListTab
