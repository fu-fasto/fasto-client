/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react"
import { Link } from "react-router-dom"
import Breacrumbs from "../../../components/@vuexy/breadCrumbs/BreadCrumb"
import FastTable from "../../../components/@fasto/FastTable"
import { useHistory } from "react-router-dom"
import { useSelector } from "react-redux"
import {
  Input,
  Button,
  Card,
  CardBody,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Spinner
} from "reactstrap"
import { Eye, Delete } from "react-feather"
import storeService from "../../../services/storeService"

const Store = () => {
  const history = useHistory()
  const [stores, setStores] = useState([])
  const [mod, setMod] = useState(false)
  const [modalData, setModalData] = useState()
  const currentUser = useSelector(x => x.auth)

  const columnStyle = [
    {
      headerName: "Store name",
      field: "name"
    },
    {
      headerName: "Address",
      field: "address"
    },
    {
      headerName: "City",
      field: "city"
    },
    {
      headerName: "District",
      field: "district"
    },
    {
      headerName: "Category",
      cellRendererFramework: ({ data }) => {
        return (
          <div className="d-flex align-items-center cursor-pointer">
            {data.categories.map(x => x.name).join(", ")}
          </div>
        )
      }
    },
    {
      headerName: "Cuisines",
      cellRendererFramework: ({ data }) => {
        return (
          <div className="d-flex align-items-center cursor-pointer">
            {data.cuisines.map(x => x.name).join(", ")}
          </div>
        )
      }
    },
    {
      header: "Action",
      field: "action",
      cellRendererFramework: ({ data }) => {
        return (
          <div className="d-flex align-items-center cursor-pointer">
            <Link to={`/app/store/detail/${data.id}`}>
              <Button className="mr-1 mb-1" size="sm" color="primary">
                <Eye size={15} />
              </Button>
            </Link>
            <Button
              className="mr-1 mb-1"
              size="sm"
              color="danger"
              onClick={() => {
                setModalData(data)
                toggleModal()
              }}
            >
              <Delete size={15} />
            </Button>
          </div>
        )
      }
    }
  ]

  const actions = [
    {
      text: "Create store",
      onClick: () =>
        history.push({
          pathname: "/app/store/create",
          state: {
            store: null,
            action: "Create"
          }
        })
    }
  ]

  const toggleModal = () => {
    setMod(!mod)
  }
  let del = ""

  const handleInputChange = event => {
    del = event.target.value
  }
  const confirmDelete = () => {
    if (del === modalData.name) {
      //request delete
      toggleModal()
      setModalData({})
    } else {
      //khong xoa
    }
    del = ""
  }

  useEffect(() => {
    storeService.getAllStore().then(response => {
      setStores(response.data.items)
    })
  }, [])

  if (currentUser?.role === "STORE.ADMIN") {
    //this is how I tried to redirect
    history.push({ pathname: `/app/store/detail/${currentUser?.storeId}` })
  }

  if (!stores) {
    return (
      <div className="text-center">
        <Spinner className="mr-50" color="primary" />
      </div>
    )
  }

  return (
    <div>
      <Card>
        <CardBody>
          <FastTable
            rowData={stores}
            columnDefs={columnStyle}
            actions={actions}
          />
        </CardBody>
        <Modal
          isOpen={mod}
          toggle={toggleModal}
          className="modal-dialog-centered"
        >
          <ModalHeader toggle={toggleModal}>
            Bạn có chắc xoá nhà hàng này?
          </ModalHeader>
          <ModalBody>
            <h3>
              Nhập "{modalData ? modalData.name : ""}" để xoá nhà hàng này
            </h3>
            <Input
              type="text"
              id="confirmDelete"
              name="confirmDelete"
              placeholder={modalData ? modalData.name : ""}
              onChange={handleInputChange}
            />
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={confirmDelete}>
              Đồng ý xoá
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </Card>

      {/* Xem danh sach voi card */}
      {/* <ListStore stores={stores} /> */}
    </div>
  )
}

export default Store
