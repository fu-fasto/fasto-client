import React, { useState, useEffect } from "react"
import { Button, Row, Col } from "reactstrap"
import { Sliders, RefreshCcw } from "react-feather"
import "rc-slider/assets/index.css"
import "../../../assets/scss/plugins/extensions/slider.scss"
import Select from "react-select"
import storeService from "../../../services/storeService"
import "../../../assets/scss/plugins/extensions/slider.scss"

const convertOptions = (array, fieldName) => {
  return array.map(item => ({
    value: item.id,
    label: `${item[fieldName]} (${item.count})`
  }))
}

const FilterCard = ({ findQuery, onFilter, applyFilter, onReset }) => {
  const [metadata, setMetadata] = useState({
    cities: [],
    districts: [],
    cuisines: [],
    categories: []
  })

  useEffect(() => {
    storeService.getMetadata(findQuery).then(response => {
      setMetadata(response.data)
    })
  }, [findQuery])

  return (
    <Row>
      <Col sm="2">
        <Select
          isClearable={true}
          options={convertOptions(metadata?.cities, "name")}
          className="React"
          classNamePrefix="select"
          id="city"
          placeholder="City"
          onChange={e => onFilter("cityId", e)}
        />
      </Col>
      <Col sm="2">
        <Select
          isClearable={true}
          options={convertOptions(metadata?.districts, "name")}
          className="React"
          classNamePrefix="select"
          id="district"
          placeholder="District"
          onChange={e => onFilter("districtId", e)}
        />
      </Col>
      <Col sm="3">
        <Select
          isClearable={true}
          options={convertOptions(metadata?.categories, "name")}
          className="React"
          classNamePrefix="select"
          id="category"
          placeholder="Category"
          onChange={e => onFilter("categories", e)}
        />
      </Col>

      <Col sm="3">
        <Select
          isClearable={true}
          options={convertOptions(metadata?.cuisines, "name")}
          className="React"
          classNamePrefix="select"
          id="cuisine"
          placeholder="Cuisines"
          onChange={e => onFilter("cuisines", e)}
        />
      </Col>

      <Col sm="1">
        <Button.Ripple
          block
          className="btn-block"
          color="primary"
          onClick={applyFilter}
        >
          <Sliders size={15} />
        </Button.Ripple>
      </Col>
      <Col sm="1">
        <Button.Ripple
          block
          className="btn-block"
          color="warning"
          onClick={onReset}
        >
          <RefreshCcw size={15} />
        </Button.Ripple>
      </Col>
    </Row>
  )
}

export default FilterCard
