import React from 'react'
import Slider from "rc-slider"

const marks = {
    0: <strong>0 VND</strong>,
    10: "100K VND",
    20: "200K VND",
    50: "500K VND",
    100: "1M VND"
  }

const SliderMark = () => {

    return (
        <Slider
            min={0}
            marks={marks}
            step={null}
            defaultValue={20}
            className="pb-2"
        />
    )
}

export default SliderMark
