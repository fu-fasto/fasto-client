import React, { useState } from "react"
// import Swiper from "react-id-swiper"
import {
  Row,
  Col,
  Badge,
  Card,
  Button,
  CardBody,
  Modal,
  ModalHeader
} from "reactstrap"
import {
  Clock,
  MapPin,
  FileMinus,
  Phone,
  Star,
  Edit,
  ShoppingCart,
  Home
} from "react-feather"
import { Link } from "react-router-dom"
import moment from "moment"
import StoreMap from "./StoreMap"

const newTime = hour => {
  const time = new Date()
  time.setHours(hour)
  time.setMinutes(0)
  time.setSeconds(0)
  return time
}

const getTime = time => {
  return moment(time).local().format("HH:mm")
}

const openStatus = (openTime, closeTime) => {
  const currentTime = new Date()

  if (openTime <= currentTime && closeTime >= currentTime) {
    return (
      <Badge color="success badge-glow" pill className="badge-md">
        <Clock size={20} /> Open
      </Badge>
    )
  } else {
    return (
      <Badge color="danger badge-glow" pill className="badge-md">
        <Clock size={20} /> Close
      </Badge>
    )
  }
}

const HeaderCard = ({ props }) => {
  const [mod, setMod] = useState(false)

  if (!props) {
    return <div></div>
  }
  const toggleModal = () => {
    setMod(!mod)
  }
  const store = props.store
  store.openTime = newTime(9)
  store.closeTime = newTime(20)

  const currentUser = props.currentUser
  return (
    <Card>
      <CardBody>
        <Row>
          <Col lg="5">
            {/* <Swiper {...params}> */}
            {store.images.map((img, i) => {
              return (
                <div key={i}>
                  <img
                    src={img}
                    //   width="600"
                    //   height="200"
                    alt={"Image" + (i + 1)}
                    className="img-fluid w-100"
                  />
                </div>
              )
            })}
            {/* </Swiper> */}
          </Col>

          <Col lg="7" className="d-flex flex-column justify-content-between">
            <Row>
              <Col>
                <h2> </h2>
              </Col>
            </Row>
            <Row>
              <Col sm="10">
                <h2 className="display-5">
                  {store.name}
                  {"    "}
                </h2>
              </Col>
              <Col sm="2">
                <Badge color="warning" pill className="badge-lg">
                  {(Math.round(store?.star * 100) / 100).toFixed(1)}{" "}
                  <Star size={15} />
                </Badge>
              </Col>
            </Row>
            <Row>
              <Col>
                <h5>
                  {" "}
                  {openStatus(store?.openTime, store?.closeTime)}{" "}
                  {getTime(store?.openTime)} - {getTime(store?.closeTime)}{" "}
                </h5>
              </Col>
            </Row>
            <Row>
              <Col>
                <h5>
                  <MapPin size={20} onClick={() => toggleModal()} />{" "}
                  {store?.address}, {store?.district}, {store?.city}
                </h5>
              </Col>
            </Row>
            <Row>
              <Col>
                <h5>
                  <FileMinus size={20} /> Type:{" "}
                  {store?.categories.reduce((acc, currentValue, currIndex) => {
                    return (
                      acc +
                      currentValue.name +
                      (currIndex === store.categories.length - 1 ? "" : ", ")
                    )
                  }, "")}
                </h5>
              </Col>
            </Row>
            <Row>
              <Col>
                <h5>
                  <Home size={20} /> Cuisines:{" "}
                  {store?.cuisines.reduce((acc, currentValue, currIndex) => {
                    return (
                      acc +
                      currentValue.name +
                      (currIndex === store.cuisines.length - 1 ? "" : ", ")
                    )
                  }, "")}
                </h5>
              </Col>
            </Row>
            <Row>
              <Col>
                <h5>
                  <Phone size={20} /> 0956 486 234
                </h5>
              </Col>
            </Row>
            <Row style={{ paddingTop: 10 }}>
              <Col sm="6" lg="4">
                <Link to={`/make-reservation/${store?.id}`}>
                  <Button className="mr-1 mb-1 w-100" color="danger">
                    <ShoppingCart size={15} />
                    <span className="align-middle ml-50">Reserve</span>
                  </Button>
                </Link>
              </Col>
              {currentUser.role === "FASTO.ADMIN" ||
              (currentUser.role === "STORE.ADMIN" &&
                currentUser.storeId === store.id) ? (
                <Col sm="6" lg="4">
                  <Link
                    to={{
                      pathname: "/app/store/edit",
                      state: { store: store, action: "Edit" }
                    }}
                  >
                    <Button className="mr-1 mb-1 w-100" color="primary">
                      <Edit size={15} />
                      <span className="align-middle ml-50">Edit store</span>
                    </Button>
                  </Link>
                </Col>
              ) : (
                <div></div>
              )}
            </Row>
          </Col>
        </Row>
      </CardBody>
      <Modal
        isOpen={mod}
        toggle={toggleModal}
        className="modal-dialog-centered"
      >
        <ModalHeader toggle={toggleModal}>Store map</ModalHeader>
        <StoreMap
          props={{
            city: store?.city,
            district: store?.district,
            address: store.address
          }}
        />
      </Modal>
    </Card>
  )
}

export default HeaderCard
