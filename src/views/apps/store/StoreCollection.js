import React from "react"
import { Card, CardImg, CardImgOverlay, Badge, Row, Col } from "reactstrap"

const collection = [
  {
    img:
      "https://images.foody.vn/bookingcollection/s624x390/beauty-upload-api-552x344-201231105234.jpg",
    title: "VIETNAM FOOD"
  },
  {
    img:
      "https://images.foody.vn/bookingcollection/s624x390/beauty-upload-api-552x344-210106172257.jpg",
    title: "KOREA FOOD"
  },
  {
    img:
      "https://images.foody.vn/bookingcollection/s624x390/beauty-upload-api-552x344-201231153531.jpg",
    title: "JAPAN FOOD"
  },
  {
    img:
      "https://images.foody.vn/bookingcollection/s624x390/beauty-upload-api-552x344-201123170500.png",
    title: "THAILAND FOOD"
  },
  {
    img:
      "https://images.foody.vn/bookingcollection/s624x390/beauty-upload-api-552-x-344-%282%29-210118133402.jpg",
    title: "CHINA FOOD"
  },
  {
    img:
      "https://images.foody.vn/bookingcollection/s624x390/beauty-upload-api-552x344-210308103211.jpg",
    title: "BEST DEAL FOR COUPLE"
  },
  {
    img:
      "https://images.foody.vn/bookingcollection/s624x390/beauty-upload-api-552x344-210120093313.jpg",
    title: "BEST SELLER"
  },
  {
    img:
      "https://images.foody.vn/bookingcollection/s624x390/beauty-upload-api-552x344_nonstopbuffee20-190704143702.jpg",
    title: "NONSTOP BUFFET"
  }
]
const StoreCollection = () => {
  return (
    <div className="ecommerce-application d-flex flex-wrap">
      {collection.map((item, i) => {
        return (
          <Col key={i} sm="6" lg="3">
            <Card
              style={{ cursor: "pointer" }}
              className="mb-1 ecommerce-card text-white overlay-img-card"
            >
              <div className="card-content">
                <CardImg className="img-fluid" src={item.img}></CardImg>
                <CardImgOverlay className="d-flex flex-column justify-content-between">
                  <Row></Row>
                  <Row></Row>
                  <Row>
                    <Badge color="dark" className="badge-md">
                      <span>{item.title}</span>
                    </Badge>
                  </Row>
                </CardImgOverlay>
              </div>
            </Card>
          </Col>
        )
      })}
    </div>
  )
}

export default StoreCollection
