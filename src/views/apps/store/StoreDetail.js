import React, { useState, useEffect } from "react"
import { useSelector } from "react-redux"
import { Row, Col, Spinner } from "reactstrap"
import HeaderCard from "./HeaderCard"
import "../../../assets/scss/pages/app-ecommerce-shop.scss"
import StoreTabContent from "./StoreTabContent"
import storeService from "../../../services/storeService"
import itemService from "../../../services/itemService"

const StoreDetail = props => {
  const [store, setStore] = useState()
  const [menu, setMenu] = useState([])
  const { id } = props.match.params
  const currentUser = useSelector(x => x.auth)
  useEffect(() => {
    storeService.getOneStore(id).then(response => {
      setStore(response.data)
    })
    itemService.getAllByStoreId(id).then(response => {
      setMenu(response.data)
    })
  }, [id])

  if (!store) {
    return (
      <div className="text-center">
        <Spinner className="mr-50" color="primary" />
      </div>
    )
  }

  return (
    <React.Fragment>
      <Row>
        <Col sm="12">
          <HeaderCard
            props={{
              imageList: store.images,
              store: store,
              currentUser: currentUser
            }}
          ></HeaderCard>
          <StoreTabContent
            store={store}
            menu={menu}
            currentUser={currentUser}
          />
        </Col>
      </Row>
    </React.Fragment>
  )
}

export default StoreDetail
