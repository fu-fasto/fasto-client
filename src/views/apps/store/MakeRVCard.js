import React from 'react'
import { Card, Badge } from "reactstrap"
import FastForm from "../../../components/@fasto/FastForm"
const MakeRVCard = () => {
    const definitions = [
        {
            headerName: "Time",
            field: "time",
            type: "datetime"
        },
        {
            headerName: "Number of customer",
            field: "customerNumber",
            type: "number"
        },
        {
            headerName: "Note",
            field: "note",
            type: "text"
        }

    ]
    const actions = [
        {
            text: "Book",
            type: "submit"
        }
    ]
    const handleSubmit = async values => {


    }
    return (
        <div>
            <Badge className="block badge-xl bg-gradient-primary">
                Make a reservation
            </Badge>
            <Card>
                <FastForm
                    definitions={definitions}
                    actions={actions}
                    onSubmit={handleSubmit}
                />
            </Card>
        </div>


    )
}

export default MakeRVCard
