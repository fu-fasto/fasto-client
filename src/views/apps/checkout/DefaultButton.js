import React from "react"
import { Button } from "reactstrap"

function DefaultButton(props) {
  const { show } = props

  if (!show) return null

  return (
    <React.Fragment>
      <Button.Ripple
        disabled
        type="submit"
        block
        color="primary"
        className="btn-block"
      >
        Payment
      </Button.Ripple>
    </React.Fragment>
  )
}

export default DefaultButton
