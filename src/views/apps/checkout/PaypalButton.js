import React from "react"
import ReactDOM from "react-dom"
import service from "../../../services/paymentService"
import rvService from "../../../services/rvService"

function PaypalButton(props) {
  const { visit, show, handleAlert } = props
  const PayPal = window.paypal.Buttons.driver("react", { React, ReactDOM })

  const handleCreateOrder = async (data, actions) => {
    const response = await rvService.createPaymentTransaction(visit.id, "PAYPAL")
    localStorage.setItem("paymentTransactionId", response.paymentTransactionId)

    return response.transactionData.paypalOrderId
  }

  const handleOnApprove = async (data, actions) => {
    const res = await service.completePaymentTransaction(
      localStorage.getItem("paymentTransactionId")
    )
    res === 200 ? handleAlert("success") : handleAlert("error")
  }

  if (!show) return null

  return (
    <React.Fragment>
      <PayPal
        createOrder={async (data, actions) => {
          return await handleCreateOrder(data, actions)
        }}
        onApprove={(data, actions) => {
          handleOnApprove(data, actions)
        }}
      />
    </React.Fragment>
  )
}

export default PaypalButton
