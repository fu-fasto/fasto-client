import React, { useState, useEffect } from "react"
import { Button } from "reactstrap"
import rvService from "../../../services/rvService"
import paymentService from "../../../services/paymentService"
import { toast } from "react-toastify"
import ReactDOM from "react-dom"

function MomoButton(props) {
  const { visit, currentProvider } = props
  const [transactionData, setTransactionData] = useState(null)

  const PayPal = window.paypal.Buttons.driver("react", { React, ReactDOM })

  const handleCreateOrder = async () => {
    const response = await rvService.createPaymentTransaction(
      visit.id,
      currentProvider
    )
    if (response.ErrorMessage) {
      toast.error(response.ErrorMessage)
    } else {
      setTransactionData(response.transactionData)
      localStorage.setItem(
        "paymentTransactionId",
        response.paymentTransactionId
      )
      return response.transactionData
    }
  }

  const handleOnApprove = async () => {
    const id = localStorage.getItem("paymentTransactionId")

    await paymentService.completePaymentTransaction(id)
  }

  useEffect(() => {
    if (transactionData) {
      switch (currentProvider) {
        case "MOMO":
          window.location.href = transactionData.payUrl
          break

        case "PAYPAL":
          break

        default:
          break
      }
    }
  }, [transactionData, currentProvider])

  return (
    <div>
      <Button
        hidden={currentProvider === "PAYPAL"}
        className="btn-block"
        color="primary"
        onClick={async () => await handleCreateOrder()}
      >
        Pay now
      </Button>

      <div hidden={currentProvider !== "PAYPAL"}>
        <PayPal
          createOrder={async () => {
            const transactionData = await handleCreateOrder()
            return transactionData.paypalOrderId
          }}
          onApprove={() => handleOnApprove()}
        />
      </div>
    </div>
  )
}

export default MomoButton
