import React from "react"
import { Card, CardBody, Button } from "reactstrap"
import "../../../assets/scss/pages/checkout.scss"

function DetailStep(props) {
  const { handleActiveStep, orders, totalPrice, storeItems } = props

  return (
    <React.Fragment>
      <div className="list-view">
        <div className="checkout-items">
          {orders.map((order, index) => (
            <Card className="ecommerce-card" key={index}>
              <div className="card-content">
                <div className="item-img text-center">
                  <img src={storeItems.find(x => x.id === order.itemId)?.imageUrl} width="150" height="150" alt="item" />
                </div>

                <CardBody>
                  <div className="item-name">
                    <span>{order.name}</span>
                  </div>
                  <div className="item-quantity">
                    <p className="quantity-title">Quantity</p>
                    <div>{order.quantity}</div>
                  </div>
                </CardBody>
                <div className="item-options text-center">
                  <div className="item-wrapper">
                    <div className="item-cost">
                      <h6 className="item-price">{order.price}</h6>
                    </div>
                  </div>
                </div>
              </div>
            </Card>
          ))}
        </div>
        <div className="checkout-summary">
          <Card>
            <CardBody>
              <div className="price-details">
                <p>Price Details</p>
              </div>
              <div className="detail">
                <div className="detail-title">Total Price</div>
                <div className="detail-amt">{totalPrice} VND</div>
              </div>
              <div className="detail">
                <div className="detail-title">Discount</div>
                <div className="detail-amt discount-amt">0 VND</div>
              </div>
              <hr />
              <div className="detail">
                <div className="detail-title detail-total">Total</div>
                <div className="detail-amt total-amt">{totalPrice} VND</div>
              </div>
              <Button
                block
                color="primary"
                className="btn-block"
                onClick={() => handleActiveStep(1)}
              >
                Continue
              </Button>
            </CardBody>
          </Card>
        </div>
      </div>
    </React.Fragment>
  )
}

export default DetailStep
