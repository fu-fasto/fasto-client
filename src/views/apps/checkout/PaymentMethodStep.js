import React, { useState } from "react"
import { Card, CardBody, CardHeader, CardTitle, Row, Col } from "reactstrap"
import "../../../assets/scss/pages/checkout.scss"
import MoMoButton from "./MomoButton"

function PaymentMethodStep({ visit, totalPrice }) {
  const [currentProvider, setCurrentProvider] = useState("PAYPAL")

  const onChangeValue = event => {
    setCurrentProvider(event.target.value.toUpperCase())
  }

  return (
    <React.Fragment>
        <Row>
          <Col xs={12} md={8}>
            <div className="checkout-items">
              <Card>
                <CardHeader>
                  <CardTitle>Payment Options</CardTitle>
                </CardHeader>
                <CardBody>
                  <div onChange={onChangeValue}>
                    <div className="vx-radio-con">
                      <input
                        name="payment-method"
                        value="paypal"
                        type="radio"
                        defaultChecked
                      ></input>
                      <span className="vx-radio">
                        <span className="vx-radio--border"></span>
                        <span className="vx-radio--circle"></span>
                      </span>
                      <img
                        src={
                          "https://cdn.iconscout.com/icon/free/png-256/paypal-32-498436.png"
                        }
                        alt="img-placeholder"
                        height="40"
                      />
                      <strong className="ml-1">PayPal</strong>
                    </div>
                    <div className="vx-radio-con">
                      <input
                        name="payment-method"
                        value="momo"
                        type="radio"
                      ></input>
                      <span className="vx-radio">
                        <span className="vx-radio--border"></span>
                        <span className="vx-radio--circle"></span>
                      </span>
                      <img
                        src={"https://static.mservice.io/img/logo-momo.png"}
                        alt="img-placeholder"
                        height="40"
                      />
                      <strong className="ml-1">MoMo</strong>
                    </div>
                  </div>
                </CardBody>
              </Card>
            </div>
          </Col>
          <Col xs={12} md={4}>
            <div className="checkout-summary">
              <Card>
                <CardBody>
                  <div className="price-details">
                    <p>Price Details</p>
                  </div>
                  <div className="detail">
                    <div className="detail-title">Total MRP</div>
                    <div className="detail-amt">{totalPrice} VND</div>
                  </div>
                  <div className="detail">
                    <div className="detail-title">Discount</div>
                    <div className="detail-amt discount-amt">0 VND</div>
                  </div>
                  <hr />
                  <div className="detail">
                    <div className="detail-title detail-total">Total</div>
                    <div className="detail-amt total-amt">{totalPrice} VND</div>
                  </div>
                  <MoMoButton visit={visit} currentProvider={currentProvider} />
                </CardBody>
              </Card>
            </div>
          </Col>
        </Row>
    </React.Fragment>
  )
}

export default PaymentMethodStep
