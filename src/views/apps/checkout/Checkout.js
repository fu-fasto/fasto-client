/* eslint-disable no-unused-vars */
import React, { useState } from "react"
import { CreditCard, DollarSign } from "react-feather"
import Wizard from "../../../components/@vuexy/wizard/WizardComponent.js"
import DetailStep from "./DetailStep"
import PaymentMethodStep from "./PaymentMethodStep"

function Checkout({ visit, distinctOrders, storeItems }) {
  const [activeStep, setActiveStep] = useState(0)
  const orderDict = distinctOrders.reduce((orderDict, order) => {
    const key = order.itemId
    if (key in orderDict) {
      orderDict[key].quantity += order.quantity
    } else {
      orderDict[key] = {
        itemId: order.itemId,
        name: order.name,
        price: order.price,
        quantity: order.quantity,
        state: order.state
      }
    }

    return orderDict
  }, {})
  const totalPrice = Object.values(orderDict).reduce((acc, order) => {
    switch (order.state) {
      case 3:
      case 4:
        acc += order.price * order.quantity
        break
      default:
        break
    }

    return acc
  }, 0)

  const handleActiveStep = index => {
    setActiveStep(index)
  }

  const steps = [
    {
      title: <DollarSign size={22} />,
      content: (
        <DetailStep
          orders={Object.values(orderDict)}
          totalPrice={totalPrice}
          storeItems={storeItems}
          handleActiveStep={handleActiveStep}
        />
      )
    },
    {
      title: <CreditCard size={22} />,
      content: (
        <PaymentMethodStep visit={visit} totalPrice={totalPrice} size={22} />
      )
    }
  ]

  return (
    <React.Fragment>
      {/* <Wizard steps={steps} activeStep={activeStep} pagination={false} /> */}
      <PaymentMethodStep visit={visit} totalPrice={totalPrice} size={22} />
    </React.Fragment>
  )
}

export default Checkout
