import mock from "../mock"

const user = {
    id: 268,
    username: "adoptionism744",
    avatar: require("../../assets/img/portrait/small/avatar-s-3.jpg"),
    email: "angelo@sashington.com",
    name: "Angelo Sashington",
    dob: "28 January 1998",
    gender: "female",
    country: "Bolivia",
    role: "admin",
    status: "active",
    is_verified: true,
    department: "sales",
    company: "WinDon Technologies Pvt Ltd",
    mobile: "+65958951757",
    website: "https://rowboat.com/insititious/Angelo",
    languages_known: ["English", "Arabic"],
    contact_options: ["email", "message", "phone"],
    location: {
        add_line_1: "A-65, Belvedere Streets",
        add_line_2: "",
        post_code: "1868",
        city: "New York",
        state: "New York",
        country: "United States"
    }
}

mock.onGet("api/users/268").reply(200, user);