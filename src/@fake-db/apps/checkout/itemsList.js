import mock from "../../mock"

const itemsList = [
  {
    name: "Bánh rán Dorayaki",
    img:
      "https://binhduongngaynay.com/uploads/images/201026163716_banh-ngot-pho-bien.webp",
    quantity: "5",
    price: 2.0
  },
  {
    name: "Muffin",
    img:
      "https://binhduongngaynay.com/uploads/images/201026163844_banh-ngot-ngon-pho-bien-nhat.webp",
    quantity: "2",
    price: 1.0
  },
  {
    name: "Cheesecake ",
    img:
      "https://binhduongngaynay.com/uploads/images/201026164620_cac-loai-banh-ngot-hot-nhat-viet-nam.webp",
    quantity: "1",
    price: 10.0
  }
]

mock.onGet("/visit/item-list").reply(200, itemsList)
