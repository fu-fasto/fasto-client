import mock from "../mock"
const user =
{
  id: 268,
  username: "adoptionism744",
  avatar: require("../../assets/img/portrait/small/avatar-s-3.jpg"),
  email: "angelo@sashington.com",
  name: "Angelo Sashington",
  dob: "1998-01-28",
  gender: "female",
  country: "Bolivia",
  role: "admin",
  status: "active",
  is_verified: true,
  department: "sales",
  company: "WinDon Technologies Pvt Ltd",
  mobile: "0359848402",
  website: "https://rowboat.com/insititious/Angelo",
  languages: ["English", "Arabic", "Spanish"],
  contact_options: ["email", "message", "phone"],
  location: {
    add_line_1: "A-65, Belvedere Streets",
    add_line_2: "",
    post_code: "1868",
    city: "New York",
    state: "New York",
    country: "United States"
  },
  social_links: {
    twitter: "https://twitter.com/adoptionism744",
    facebook: "https://www.facebook.com/adoptionism664",
    instagram: "https://www.instagram.com/adopt-ionism744/",
    github: "https://github.com/madop818",
    codepen: "https://codepen.io/adoptism243",
    slack: "@adoptionism744"
  },
  permissions: {
    users: {
      read: true,
      write: false,
      create: false,
      delete: false
    },
    posts: {
      read: true,
      write: true,
      create: true,
      delete: true
    },
    comments: {
      read: true,
      write: false,
      create: true,
      delete: false
    }
  }
}

// GET DATA
mock.onGet("/api/user").reply(200, user)