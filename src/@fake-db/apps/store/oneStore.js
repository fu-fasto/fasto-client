import mock from "../../mock"

const store =
    {
        id: 1,
        name: "Ba lu",
        image: require("../../../assets/img/pages/3-convex.png"),
        openTime: "15:00:00",
        closeTime: "24:00:00",
        address: "03 Nguyen Dinh Hien",
        location: {
            city: "Da nang",
            district: "Ngu Hanh Son",
            ward: "Hoa Hai"
        },
        category: "Quan Nhau",
        description: "Không chỉ là món ăn đặc sản bổ dưỡng, ba ba còn được coi là vị thuốc quý trong y học cổ truyền bởi tính bình và vị ngọt, giúp hồi phục sức khỏe, bổ huyết... Đặc biệt, ba ba khi được chế biến đúng cách, kết hợp hài hòa với gia vị còn tạo nên những món ăn rất thơm ngon và lôi cuốn. Bởi lẽ đó, các món ăn từ ba ba hiện được rất nhiều thực khách ưa chuộng. Nhận thấy nhu cầu của thực khách, đồng thời mong muốn tạo ra những không gian ẩm thực chất lượng, không chỉ thỏa mãn nhu cầu ăn ngon mà còn đáp ứng được nhiều mục đích đi ăn, liên hoan, gặp gỡ... hệ thống nhà hàng Ba Ba Sơn Đông đã ra đời.",
        star: 4,
        menu: [
            {
                id: 1,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            },
            {
                id: 2,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            },
            {
                id: 3,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            },
            {
                id: 4,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            } 
        ]
    }

// GET DATA
mock.onGet("/api/store/oneStore").reply(200, store)