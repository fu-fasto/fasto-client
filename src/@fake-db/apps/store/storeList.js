import mock from "../../mock"
const stores = [
    {
        id: 1,
        name: "Ba lu",
        image: require("../../../assets/img/pages/3-convex.png"),
        openTime: "7:00:00",
        closeTime: "12:00:00",
        address: "03 Nguyen Dinh Hien",
        location: {
            city: "Đà Nẵng",
            district: "Ngũ Hành Sơn",
            ward: "Hoà Hải"
        },
        star: 4,
        category: "Quan Nhau",
        menu: [
            {
                id: 1,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            },
            {
                id: 2,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            },
            {
                id: 3,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            }  
        ]
    },
    {
        id: 2,
        name: "Mục Đồng",
        image: require("../../../assets/img/pages/3-convex.png"),
        openTime: "15:00:00",
        closeTime: "24:00:00",
        address: "03 Nguyen Dinh Hien",
        location: {
            city: "Đà Nẵng",
            district: "Ngũ Hành Sơn",
            ward: "Hoà Hải"
        },
        star: 3,
        category: "Quan Nhau",
        menu: [
            {
                id: 4,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            },
            {
                id: 5,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            },
            {
                id: 6,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            }  
        ]
    },
    {
        id: 3,
        name: "Bún bò",
        image: require("../../../assets/img/pages/3-convex.png"),
        openTime: "15:00:00",
        closeTime: "24:00:00",
        address: "03 Nguyen Dinh Hien",
        location: {
            city: "Đà Nẵng",
            district: "Ngũ Hành Sơn",
            ward: "Hoà Hải"
        },
        star: 2,
        category: "Quan Nhau",
        menu: [
            {
                id: 7,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            },
            {
                id: 8,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            },
            {
                id: 9,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            }  
        ]
    },
    {
        id: 4,
        name: "Bún chả",
        image: require("../../../assets/img/pages/3-convex.png"),
        openTime: "15:00:00",
        closeTime: "24:00:00",
        address: "03 Nguyen Dinh Hien",
        location: {
            city: "Đà Nẵng",
            district: "Ngũ Hành Sơn",
            ward: "Hoà Hải"
        },
        star: 5,
        category: "Quan Nhau",
        menu: [
            {
                id: 10,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            },
            {
                id: 11,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            },
            {
                id: 12,
                storeId: 1,
                name: "Ech xao",
                price: 30000,
                image: require("../../../assets/img/pages/3-convex.png")
            }  
        ]
    }
]

// GET DATA
mock.onGet("/api/store/list").reply(200, stores)