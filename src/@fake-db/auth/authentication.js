import mock from "../mock"
import jwt from "jsonwebtoken"

let users = [
  {
    id: "45132213-55de-433f-8045-d83654f77sss",
    name: "FastO Admin",
    email: "admin@fasto.com",
    password: "123456",
    role: "FASTO.ADMIN"
  },
  {
    id: "45132213-55de-433f-8045-d83654f77ebf",
    name: "Store Admin",
    email: "store@fasto.com",
    password: "123456",
    role: "STORE.ADMIN",
    storeId: "12863785-165d-4bf0-a8b4-2c6b608595dc"
  },
  {
    id: "45131213-55de-433f-8045-d83654f77ebf",
    name: "Store Staff",
    email: "staff@fasto.com",
    password: "123456",
    role: "STORE.STAFF",
    storeId: "12863785-165d-4bf0-a8b4-2c6b608595dc"
  },
  {
    id: "12345678-55de-433f-8045-d83654f77ebf",
    name: "Customer",
    email: "customer@fasto.com",
    password: "123456",
    role: "CUSTOMER"
  },
  {
    id: "33211234-55de-433f-8045-d83654f77ebf",
    name: "Customer 1",
    email: "customer1@fasto.com",
    password: "123456",
    role: "CUSTOMER"
  }
]

const jwtConfig = {
  secret: "SUPER-SECRET-KEY-FASTO",
  expireTime: 800000
}

// POST: Check User Login Details and return user
mock.onPost("/auth/login").reply(request => {
  let { email, password } = JSON.parse(request.data)
  let error = "Something went wrong"
  const user = users.find(
    user => user.email === email && user.password === password
  )

  if (user) {
    try {
      const accessToken = jwt.sign(
        { ...user, uid: user.id },
        jwtConfig.secret,
        {
          expiresIn: jwtConfig.expireTime
        }
      )

      const userData = Object.assign({}, user)

      delete userData.password

      const response = {
        ...userData,
        accessToken: accessToken,
        refreshToken: accessToken
      }

      return [200, response]
    } catch (e) {
      error = e
    }
  } else {
    error = "Email Or Password Invalid"
  }

  return [404, { error }]
})

mock.onPost("/api/authenticate/register/user").reply(request => {
  if (request.data.length > 0) {
    let { email, password, name } = JSON.parse(request.data)
    const isEmailAlreadyInUse = users.find(user => user.email === email)
    const error = {
      email: isEmailAlreadyInUse ? "This email is already in use." : null,
      name: name === "" ? "Please enter your name." : null
    }

    if (!error.name && !error.email) {
      let userData = {
        email: email,
        password: password,
        name: name
      }

      // Add user id
      const length = users.length
      let lastIndex = 0
      if (length) {
        lastIndex = users[length - 1].id
      }
      userData.id = lastIndex + 1

      users.push(userData)

      const accessToken = jwt.sign({ id: userData.id }, jwtConfig.secret, {
        expiresIn: jwtConfig.expireTime
      })

      let user = Object.assign({}, userData)
      delete user["password"]
      const response = { user: user, accessToken: accessToken }

      return [200, response]
    } else {
      return [200, { error }]
    }
  }
})

mock.onPost("/auth/refresh-token").reply(request => {
  const { accessToken } = JSON.parse(request.data)

  try {
    const { id } = jwt.verify(accessToken, jwtConfig.secret)

    let userData = Object.assign(
      {},
      users.find(user => user.id === id)
    )

    const newAccessToken = jwt.sign({ uid: userData.id }, jwtConfig.secret, {
      expiresIn: jwtConfig.expiresIn
    })

    delete userData["password"]
    const response = {
      userData: userData,
      accessToken: newAccessToken
    }

    return [200, response]
  } catch (e) {
    const error = "Invalid access token"
    return [401, { error }]
  }
})
