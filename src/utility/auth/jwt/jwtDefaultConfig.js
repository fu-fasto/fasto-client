const baseURL = process.env.REACT_APP_USER_MICROSERVICE || null

// ** Auth Endpoints
export default {
  loginEndpoint: baseURL + "/auth/login",
  registerEndpoint: baseURL + "/auth/register",
  refreshEndpoint: baseURL + "/auth/refresh-token",
  logoutEndpoint: baseURL + "/auth/logout",

  // ** This will be prefixed in authorization header with token
  // ? e.g. Authorization: Bearer <token>
  tokenType: "Bearer",

  // ** Value of this property will be used as key to store JWT token in storage
  storageTokenKeyName: "accessToken",
  storageRefreshTokenKeyName: "refreshToken"
}
