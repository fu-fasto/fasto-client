/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react"
import {
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Media,
  Badge
} from "reactstrap"
import PerfectScrollbar from "react-perfect-scrollbar"
import * as Icon from "react-feather"
import classnames from "classnames"
import Autocomplete from "../../../components/@vuexy/autoComplete/AutoCompleteComponent"
import { history } from "../../../history"
import storeService from "../../../services/storeService"
import { useHistory } from "react-router-dom"
import { useSelector } from "react-redux"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faQrcode } from "@fortawesome/free-solid-svg-icons"

// eslint-disable-next-line no-unused-vars
const NotificationDropDown = () => {
  return (
    <UncontrolledDropdown tag="li" className="dropdown-notification nav-item">
      <DropdownToggle tag="a" className="nav-link nav-link-label">
        <Icon.Bell size={21} />
        <Badge pill color="primary" className="badge-up">
          5
        </Badge>
      </DropdownToggle>
      <DropdownMenu tag="ul" right className="dropdown-menu-media">
        <li className="dropdown-menu-header">
          <div className="dropdown-header mt-0">
            <span className="notification-title">Notifications</span>
          </div>
        </li>
        <PerfectScrollbar
          className="media-list overflow-hidden position-relative"
          options={{
            wheelPropagation: false
          }}
        >
          <div className="d-flex justify-content-between">
            <Media className="d-flex align-items-start">
              <Media left href="#">
                <Icon.PlusSquare className="font-medium-5 primary" size={21} />
              </Media>
              <Media body>
                <Media heading className="primary media-heading" tag="h6">
                  You have new order!
                </Media>
                <p className="notification-text">
                  Are your going to meet me tonight?
                </p>
              </Media>
              <small>
                <time
                  className="media-meta"
                  dateTime="2015-06-11T18:29:20+08:00"
                >
                  9 hours ago
                </time>
              </small>
            </Media>
          </div>
          <div className="d-flex justify-content-between">
            <Media className="d-flex align-items-start">
              <Media left href="#">
                <Icon.DownloadCloud
                  className="font-medium-5 success"
                  size={21}
                />
              </Media>
              <Media body>
                <Media heading className="success media-heading" tag="h6">
                  99% Server load
                </Media>
                <p className="notification-text">You got new order of goods?</p>
              </Media>
              <small>
                <time
                  className="media-meta"
                  dateTime="2015-06-11T18:29:20+08:00"
                >
                  5 hours ago
                </time>
              </small>
            </Media>
          </div>
          <div className="d-flex justify-content-between">
            <Media className="d-flex align-items-start">
              <Media left href="#">
                <Icon.AlertTriangle
                  className="font-medium-5 danger"
                  size={21}
                />
              </Media>
              <Media body>
                <Media heading className="danger media-heading" tag="h6">
                  Warning Notification
                </Media>
                <p className="notification-text">Server has used 99% of CPU</p>
              </Media>
              <small>
                <time
                  className="media-meta"
                  dateTime="2015-06-11T18:29:20+08:00"
                >
                  Today
                </time>
              </small>
            </Media>
          </div>
          <div className="d-flex justify-content-between">
            <Media className="d-flex align-items-start">
              <Media left href="#">
                <Icon.CheckCircle className="font-medium-5 info" size={21} />
              </Media>
              <Media body>
                <Media heading className="info media-heading" tag="h6">
                  Complete the task
                </Media>
                <p className="notification-text">
                  One of your task is pending.
                </p>
              </Media>
              <small>
                <time
                  className="media-meta"
                  dateTime="2015-06-11T18:29:20+08:00"
                >
                  Last week
                </time>
              </small>
            </Media>
          </div>
          <div className="d-flex justify-content-between">
            <Media className="d-flex align-items-start">
              <Media left href="#">
                <Icon.File className="font-medium-5 warning" size={21} />
              </Media>
              <Media body>
                <Media heading className="warning media-heading" tag="h6">
                  Generate monthly report
                </Media>
                <p className="notification-text">
                  Reminder to generate monthly report
                </p>
              </Media>
              <small>
                <time
                  className="media-meta"
                  dateTime="2015-06-11T18:29:20+08:00"
                >
                  Last month
                </time>
              </small>
            </Media>
          </div>
        </PerfectScrollbar>
        <li className="dropdown-menu-footer">
          <DropdownItem tag="a" className="p-1 text-center">
            <span className="align-middle">Read all notifications</span>
          </DropdownItem>
        </li>
      </DropdownMenu>
    </UncontrolledDropdown>
  )
}

const UserDropdown = props => {
  return (
    <DropdownMenu right>
      <DropdownItem tag="a" href="/edit-info">
        <Icon.User size={14} className="mr-50" />
        <span className="align-middle">Edit Profile</span>
      </DropdownItem>
      <DropdownItem tag="a" href="/change-pwd">
        <Icon.User size={14} className="mr-50" />
        <span className="align-middle">Password</span>
      </DropdownItem>
      <DropdownItem divider />
      <DropdownItem
        tag="a"
        href="#"
        onClick={e => history.push("/logout")}
      >
        <Icon.Power size={14} className="mr-50" />
        <span className="align-middle">Log Out</span>
      </DropdownItem>
    </DropdownMenu>
  )
}

const convertSuggestData = rawData => {
  return rawData.map(item => ({
    id: item.id,
    title: item.name,
    link: "/store/".concat(item.id),
    icon: "Home"
  }))
}

const NavbarUser = ({ props }) => {
  const history = useHistory()
  const user = useSelector(x => x.auth)
  const [navbarSearch, setNavbarSearch] = useState(false)
  const [searchValue, setSearchValue] = useState()
  const [suggestionsList, setSuggestionsList] = useState({
    groupTitle: "Store Result:",
    searchLimit: 5,
    data: []
  })

  useEffect(() => {
    storeService.searchStore({ text: searchValue }).then(res => {
      setSuggestionsList({
        ...suggestionsList,
        data: convertSuggestData(res.data.items)
      })
    })
  }, [searchValue])

  if (!props) {
    return <div></div>
  }

  const handleNavbarSearch = () => {
    setNavbarSearch(!navbarSearch)
  }

  const searchStores = () => {
    setNavbarSearch(false)
    history.push({
      pathname: `/app/store/search`,
      search: new URLSearchParams({ text: searchValue }).toString()
    })
  }

  const userRendered = () => {
    if (user.role === "GUEST") {
      return (
        <NavItem className="nav-item mr-1">
          <NavLink
            className="nav-link"
            onClick={() => {
              history.push("/login")
            }}
          >
            <Icon.LogIn size={21} data-tour="search" />
          </NavLink>
        </NavItem>
      )
    } else {
      return (
        <React.Fragment>
          <UncontrolledDropdown
            hidden={!(user.role === "CUSTOMER")}
            tag="li"
            className="dropdown-notification nav-item"
          >
            <DropdownToggle tag="a" className="nav-link nav-link-label">
              <FontAwesomeIcon
                icon={faQrcode}
                size="lg"
                onClick={() => history.push("check-in-qr")}
              />
            </DropdownToggle>
          </UncontrolledDropdown>

          <UncontrolledDropdown tag="li" className="dropdown-user nav-item">
            <DropdownToggle tag="a" className="nav-link dropdown-user-link">
              <div className="user-nav d-sm-flex d-none">
                <span className="user-name text-bold-600">
                  {props.userName}
                </span>
                <span className="user-status">{props.role}</span>
              </div>
              <span data-tour="user">
                <img
                  src={props.userImg}
                  className="round"
                  height="40"
                  width="40"
                  alt="avatar"
                />
              </span>
            </DropdownToggle>
            <UserDropdown {...props} />
          </UncontrolledDropdown>
        </React.Fragment>
      )
    }
  }

  return (
    <ul className="nav navbar-nav navbar-nav-user float-right">
      <NavItem className="nav-search">
        <NavLink className="nav-link-search" onClick={handleNavbarSearch}>
          <Icon.Search size={21} data-tour="search" />
        </NavLink>
        <div
          className={classnames("search-input", {
            open: navbarSearch,
            "d-none": navbarSearch === false
          })}
        >
          <div className="search-input-icon">
            <Icon.Search size={17} className="primary" />
          </div>
          <Autocomplete
            className="form-control"
            suggestions={[suggestionsList]}
            filterKey="title"
            filterHeaderKey="groupTitle"
            grouped={true}
            placeholder="Enter to search..."
            autoFocus={true}
            clearInput={navbarSearch}
            externalClick={e => {
              setNavbarSearch(false)
            }}
            onKeyDown={e => {
              if (e.keyCode === 27 || e.keyCode === 13) {
                setNavbarSearch(false)
                searchStores()
              }
            }}
            onChange={e => {
              setSearchValue(e.target.value)
            }}
            customRender={(
              item,
              i,
              filteredData,
              activeSuggestion,
              onSuggestionItemClick,
              onSuggestionItemHover
            ) => {
              const IconTag = Icon[item.icon ? item.icon : "X"]
              return (
                <li
                  className={classnames("suggestion-item", {
                    active: filteredData.indexOf(item) === activeSuggestion
                  })}
                  key={i}
                  onClick={e =>
                    history.push({
                      pathname: "/app/store/detail/".concat(item.id)
                    })
                  }
                  onMouseEnter={() =>
                    onSuggestionItemHover(filteredData.indexOf(item))
                  }
                >
                  <div
                    className={classnames({
                      "d-flex justify-content-between align-items-center":
                        item.file || item.img
                    })}
                  >
                    <div className="item-container d-flex">
                      {item.icon ? (
                        <IconTag size={17} />
                      ) : item.file ? (
                        <img
                          src={item.file}
                          height="36"
                          width="28"
                          alt={item.title}
                        />
                      ) : item.img ? (
                        <img
                          className="rounded-circle mt-25"
                          src={item.img}
                          height="28"
                          width="28"
                          alt={item.title}
                        />
                      ) : null}
                      <div className="item-info ml-1">
                        <p className="align-middle mb-0">{item.title}</p>
                        {item.by || item.email ? (
                          <small className="text-muted">
                            {item.by ? item.by : item.email ? item.email : null}
                          </small>
                        ) : null}
                      </div>
                    </div>
                    {item.size || item.date ? (
                      <div className="meta-container">
                        <small className="text-muted">
                          {item.size ? item.size : item.date ? item.date : null}
                        </small>
                      </div>
                    ) : null}
                  </div>
                </li>
              )
            }}
            onSuggestionsShown={userInput => {
              if (navbarSearch) {
                // this.props.handleAppOverlay(userInput)
              }
            }}
          />
          {/* <div style={{ padding: 5 }}>
            <Input
              bsSize="lg"
              className="form-control"
              onChange={(e) => {
                setSearchValue(e.target.value)

              }}
              placeholder="Enter store name..."
              onKeyUp={e => {
                if (e.keyCode === 13) {
                  setNavbarSearch(false)
                  searchStores()
                }
              }}
            />
          </div> */}

          <div className="search-input-close">
            <Icon.ArrowLeft
              size={24}
              // onClick={() => setSearchValue("")}
            />
            <Icon.X
              size={24}
              onClick={e => {
                e.stopPropagation()
                setNavbarSearch(false)
                props.handleAppOverlay("")
              }}
            />
          </div>
        </div>
      </NavItem>
      {userRendered()}
    </ul>
  )
}
export default NavbarUser
